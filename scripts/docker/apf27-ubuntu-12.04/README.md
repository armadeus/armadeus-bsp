# Docker image for apf27 bsp

## Build
To build it :

```
sudo docker build -t apf27bsp:1.0 .
```

## Launch

To launch it dont forget to mount downloads directory and your bsp directory :

```
sudo docker run --mount src=/downloads,target=/downloads,type=bind \
                --mount src=/home/user/prj/armadeusbsp,target=/opt/bsp/,type=bind \
                -it apf27bsp:1.0 /bin/bash
```
