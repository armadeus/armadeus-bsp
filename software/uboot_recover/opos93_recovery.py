#
#  OPOS93/OPOS93SP bootloader recovering tool
#
#  Copyright (C) 2024 Armadeus Project
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# authors: sebastien.szymanski@armadeus.com
#
#

import os
import re
import serial
import subprocess
import sys
import time

from helpers import color


class UBoot(object):

    def __init__(self, serial):
        self.serial = serial

    def __getOutput(self,  pattern=r'BIOS>'):
        response = ""
        while not re.search(pattern, response):
            char = self.serial.read(1)
            if char == '':
                raise IOError
            response += char.decode("ascii", 'replace')
        print(response)
        return response

    def console_write(self, command):
        self.serial.write(command.encode("ascii"))

    def startFastboot(self):
        self.serial.flushInput()
        self.console_write("fastboot usb 0\n")
        self.serial.flushInput()

    def stopFastboot(self):
        self.serial.write(b'\x03')
        self.serial.write(b'\x03')
        ret = self.__getOutput()
        self.serial.flushInput()
        return ret

    def waitForFastbootDone(self):
        ret = self.__getOutput(r'finished')
        self.serial.flushInput()
        return ret

    def resetEnv(self,  save=True):
        if save is False:
            self.console_write("env default -f -a\n")
        else:
            self.console_write("run flash_reset_env\n")
        return self.__getOutput()

    def waitForPrompt(self):
        self.console_write("\n")
        ret = self.__getOutput()
        self.serial.flushInput()
        return ret

    def flash_uboot(self):
        self.serial.flushInput()
        self.console_write("run flash_uboot\n")
        return self.__getOutput()

    def reset(self):
        self.serial.flushInput()
        self.console_write("reset\n")


if __name__ == "__main__":

    BR_HOST_PATH = os.getenv("BR_HOST_PATH", "../../buildroot/output/host")
    USB_DOWNLOAD_APP = BR_HOST_PATH + "/usr/bin/uuu"
    IS_OPOS93SP = os.path.basename(__file__).split('_')[0] == "opos93sp"
    BOARD_NAME = "opos93sp" if IS_OPOS93SP else "opos93"
    U_BOOT_BIN = "{}-u-boot.bin".format(BOARD_NAME)
    SPEED = 115200

    print("{}\n--- {} Bootstrap Tool ---\n".format(color.BOLD, BOARD_NAME.upper()))
    print("Procedure to follow:")
    print("1] Be sure to:")
    print("   * have built the {} BSP (make {}-legacy-6.1_defconfig; make),".format(BOARD_NAME.upper(), BOARD_NAME))
    print("   * have launched this script as root (sudo).")
    print("2] Power off your board, close all serial terminal sessions, remove all USB cables")
    print("3] Put a jumper on the JP3 header.")
    print("4] Power on your board")
    print("5] Connect a USB cable from your PC to the {}Dev OTG microUSB connector".format(BOARD_NAME.upper()))
    print("")
    port = input('--- Then, enter serial port number or device name to use for console (/dev/ttyUSB0 under Linux (default) or COMx under Window$): ' + color.END)
    if port == '':
        port = '/dev/ttyUSB0'

    if not os.path.exists(USB_DOWNLOAD_APP):
        print("{}Error: uuu is not present. Please compile Armadeus BSP first: make {}-legacy-6.1_defconfig; make{}".format(color.RED, BOARD_NAME, color.END))
        sys.exit(1)

    if not os.path.exists(U_BOOT_BIN):
        print(color.RED + "Error: U-Boot binaries don't exist !" + color.END)
        sys.exit(1)

    try:
        subprocess.check_call([USB_DOWNLOAD_APP, "SDPS:", "boot", "-f", U_BOOT_BIN])
    except subprocess.CalledProcessError:
        print(color.RED + "Error: Loading U-Boot binary failed !" + color.END)
        sys.exit(1)

    try:
        print("Opening serial port {}...".format(port))
        ser = serial.Serial(port, SPEED, timeout=5)
        ser.flush()
        uboot = UBoot(ser)
        time.sleep(1)

        uboot.waitForPrompt()
        uboot.startFastboot()

        time.sleep(1)

        try:
            subprocess.check_call([USB_DOWNLOAD_APP, "FB:", "download", "-f", U_BOOT_BIN])
        except subprocess.CalledProcessError:
            print(color.RED + "Error: Loading U-Boot binary failed !" + color.END)
            sys.exit(1)

        uboot.waitForFastbootDone()
        uboot.stopFastboot()
        uboot.flash_uboot()
        uboot.waitForPrompt()

        eraseAll = input(color.BOLD + '\n--- Would you like to erase the environment variables ? y/N: ' + color.END)
        if eraseAll == 'y':
            uboot.resetEnv()
    except serial.SerialException:
        print("Error: Opening serial port {} failed !".format(port))
        sys.exit(1)
    except IOError:
        print("IOError on serial port {}".format(port))
        ser.close()
        sys.exit(1)
    except subprocess.CalledProcessError:
        print(color.RED + "Error: Loading U-Boot binaries on serial port failed !" + color.END)
        ser.close()
        sys.exit(1)
    else:
        print(color.GREEN + "\n--- U-Boot successfully recovered !" + color.END)
        print(color.BOLD +  "--- Now you can remove the microUSB OTG cable and the jumper on the JP3 header. Your board will boot...")
        print("--- As the restored U-Boot version may differ from the one your were using, " + color.UNDERLINE + "if you want to maximize the chances to keep your rootfs data safe, we strongly advise you to reflash the same U-Boot version your were using before the recovery. Otherwise (you don't care about your data), please upgrade to the latest available stable Armadeus U-Boot release." + color.END)
