#
#  APF27 bootloader recovering tool
#
#  Copyright (C) 2008-2023  Armadeus Systems <support@armadeus.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software 
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# 
# author: nicolas.colombain@armadeus.com
#
# Change History:
#  - (Sébastien Szymanski) : python3 support and cleanup
#  - (Jorasse) : init DDR RAM and u-boot upload
#

import os
import re
import serial
import sys
import time


class Error(Exception):
    """ Manage specific error

    attributes:
        message -- the message
        level   -- the exception level
    """

    def __init__(self,message,level=0):
        self._message = message
        self.level   = level
        Exception.__init__(self,message)
                
    def __repr__(self):
        return self.message

    def _get_message(self): 
        return self._message
    def _set_message(self, message): 
        self._message = message
    message = property(_get_message, _set_message)

    def __str__(self):
        print(self.message)

    def setLevel(self,level):
        self.level = int(str(level))
    def getLevel(self):
        return self.level


class apf27Bootloader:

    statuscode = {  0x8D: 'data specified is out of bounds',
                    0x55: 'error during assert verification',
                    0x36: 'hash verification failed',
                    0x33: 'certificate parsing failed or the certificate contained an unsupported key',
                    0x35: 'signature verification failed',
                    0x4B: 'CSF command sequence contains unsupported command identifier',
                    0x4E: 'absence of expected CSF header',
                    0x4D: 'CSF length is unsupported',
                    0x2E: 'CSF TYPE does not match processor TYPE',
                    0x2D: 'CSF UID does not match either processor UID or generic UID',
                    0x3A: 'CSF customer/product code does not match processor customer/product code',
                    0x87: 'key indexis either unsupported, or an attempt is made to overwrite the SRK from a CSF command',
                    0xF0: 'successful operation completion',
                    0x17: 'SCC unexpectedly not in secure state',
                    0x1E: 'secureRAM secret key invalid',
                    0x1D: 'secureRAM initialization failure',
                    0x1B: 'secureRAM self test failure',
                    0x2B: 'secureRAM internal failure',
                    0x27: 'secureRAM secrect key unexpectedly in use',
                    0x8B: 'an attempt is made to read a key from the list of subordinate public keys at a location where no key is installed',
                    0x8E: 'algorithm type is either invalid or ortherwise unsupported',
                    0x66: 'write operation to register failed',
                    0x63: 'DCD invalid',
                    0x6F: 'RAM application pointer is NULL or ERASED_FLASH',
                    0x69: 'CSF missing when HAB TYPE is not HAB-disabled',
                    0x6A: 'NANDFC boot buffer load failed',
                    0x6C: 'Exception has occured',
                    0x67: 'INT_BOOT fuse is blown but BOOT pins are set for external boot',
                    0x88: 'Successful download completion' }

    def __init__(self, serialport):
        self.port = serialport

    def getstatus(self):
        self.port.flushInput()
        self.port.write(self.buildcmd(0x0505, 0, 0, 0, 0, 0))
        result = self.port.read(4)
        if len(result) > 0:
            try:
                print(self.statuscode[result[0]])
            except KeyError as e:
                print("Unknown status {}".format(result[0]))
        else:
            print('\nread timeout')

    def __displayProgress(self, text):
        sys.stdout.write(chr(13))
        print(text, end=' ')
        sys.stdout.flush()

# addr: address where the data have to be written
# data: data to write. must match the given mode
# mode: 0x08, 0x10, 0x20 for byte, word, long

    def put(self, addr, data, mode):
        self.port.write(self.buildcmd(0x0202, addr, mode, 0, data, 0))
        result = self.port.read(4) # ack
        if len(result) == 0:
            print('read timeout')
            return

        result = self.port.read(4)
        if len(result) == 0:
            print('read timeout')

    def download(self, addr, data, size):
        assert size > 0, 'file empty'
        f=open(data, 'rb')
        self.port.write(self.buildcmd(0x0404, addr, 0, size, 0, 0xAA))
        result = self.port.read(4) # ack
        if len(result) == 0:
            print('\nread timeout')
            return

        data = f.read(16)
        count = 16
        b = 0
        print('\nTransfering:')
        while len(data):
            n = len(data)
            b = 16-n  # ensure always 4 bytes
            #tr = unpack('B'*n,data)
            self.port.write(data)
            count = count + 16
            if (count % 1024) == 0:
                self.__displayProgress("%06d bytes" % count)
            data = f.read(16)
            time.sleep(0.001)
        self.__displayProgress("%d bytes transfered" % count)
        print("")
        f.close()
        self.getstatus()

    def buildcmd(self, cmd, addr, ds, cnt, data, ft):
        result = b''.join([cmd.to_bytes(2, 'big'), addr.to_bytes(4, 'big'),
                           ds.to_bytes(1, 'big'), cnt.to_bytes(4, 'big'),
                           data.to_bytes(4, 'big'), ft.to_bytes(1, 'big')])
        return result

    def initSDRAM2( self ):
        print("SDRAM INIT2 APF", end=' ')
        self.put(0xD800100C, 0x0069D42A, 0x20)

        self.put(0xD8001008, 0x92126485, 0x20)
        self.put(0xB0000400, 0x00000000, 0x20)
        self.put(0xD8001008, 0xA2126485, 0x20)
        self.put(0xB0000000, 0x00000000, 0x20)
        self.put(0xB0000000, 0x00000000, 0x20)
        self.put(0xB0000000, 0x00000000, 0x20)
        self.put(0xB0000000, 0x00000000, 0x20)
        self.put(0xD8001008, 0xB2126485, 0x20)
        self.put(0xB0000033, 0xDA0000DA, 0x08)
        self.put(0xB2000000, 0x00000000, 0x08)
        self.put(0xD8001008, 0x82126485, 0x20)

    def initSDRAM1( self ):
        print("SDRAM INIT1 APF", end=' ')
        self.put(0x10000000, 0x20040304, 0x20) #/* AIPI1_PSR0 */
        self.put(0x10000004, 0xDFFBFCFB, 0x20) #/* AIPI1_PSR1 */
        self.put(0x10020000, 0x00000000, 0x20) #/* AIPI2_PSR0 */
        self.put(0x10020004, 0xFFFFFFFF, 0x20) #/* AIPI2_PSR1 */
        self.put(0x10027818, 0x0003000F, 0x20) #/* GPCR */

        self.put(0xD8002060, 0x00002200, 0x20) #/* EIM / WCR */

        self.put(0xD8001010, 0x0000002C, 0x20)

        self.put(0xD8001004, 0x0069D42A, 0x20)

        self.put(0xD8001000, 0x92126485, 0x20)
        self.put(0xA0000400, 0x00000000, 0x20)
        self.put(0xD8001000, 0xA2126485, 0x20)
        self.put(0xA0000000, 0x00000000, 0x20)
        self.put(0xA0000000, 0x00000000, 0x20)
        self.put(0xA0000000, 0x00000000, 0x20)
        self.put(0xA0000000, 0x00000000, 0x20)
        self.put(0xD8001000, 0xB2126485, 0x20)
        self.put(0xA0000033, 0xDA0000DA, 0x08)
        self.put(0xA2000000, 0x00000000, 0x08)
        self.put(0xD8001000, 0x82126485, 0x20)


class UBoot:
    """
    """
    def __init__(self, bootstrap, serial, binary):
        self.bootstrap = bootstrap
        self.serial = serial
        self.binary = binary

    def __getOutput(self,error_msg=None):
        """ This function waits for U-Boot response and return response"""
        response = ""
        while not re.search(r'BIOS>',response):
            char = self.serial.read(1)
            if char == '':
                raise Error(response)
            response = response + char.decode("ascii")
            if error_msg!=None:
                if re.search(error_msg,response):
                    raise Error("getOutput error")
            sys.stdout.write(char.decode("ascii"))
        return response
 
    def load(self):
        if not os.path.exists(self.binary):
            raise Error("file "+self.binary+" doesn't exit",0)
        self.fsize = os.path.getsize(self.binary)
        self.bootstrap.download(0xA0000000, self.binary, self.fsize)

    def resetenv(self):
        self.serial.flushInput()
        self.serial.write("env default -f && saveenv && echo Flash environment variables erased!\n".encode("ascii"))
        return self.__getOutput()

    def waitForPrompt(self):
        self.serial.write("\003".encode("ascii"))
        while not self.serial.inWaiting():
            time.sleep(0.001)
            self.serial.write("\003".encode("ascii"))
        return self.__getOutput()

    def flash(self):
        """ flash uboot """
        self.serial.flushInput()
        self.serial.write("setenv fileaddr A0000000; setenv filesize {:08x};run flash_uboot\n".format(self.fsize).encode("ascii"))
        try:
            ret = self.__getOutput()
        except Error as e:
            raise Error("Flashing U-Boot")
        if not re.search(r'uboot succeed', ret):
            print(ret)
            raise Error("U-Boot flash failed", 0)
        return ret


if __name__ == "__main__":    

    RECOVERY_URL = "http://sourceforge.net/projects/armadeus/files/armadeus/"
    RECOVERY_VERSION = "armadeus-5.2"
    RECOVERY_BOARD_PATH = "binaries/apf27/"
    UBOOT_IMAGE_BIN = "apf27-u-boot-nand.bin"
    UBOOT_RECOVERY_PATH = RECOVERY_URL+RECOVERY_VERSION+"/"+RECOVERY_BOARD_PATH+UBOOT_IMAGE_BIN+"/download"
    if not os.path.exists(UBOOT_IMAGE_BIN):
        try:
            print("Downloading " + UBOOT_IMAGE_BIN + "..")
            os.system("wget -O "+ UBOOT_IMAGE_BIN + " " +UBOOT_RECOVERY_PATH)
        except Exception as msg:
            print("Failed to download U-Boot image file from server!")
            print(msg)
            print("Server may be unavailable for the momenet. please try later.")
            os.remove(UBOOT_IMAGE_BIN);
            sys.exit()

    SPEED = 115200

    print("\nAPF27 Bootstrap Tool\n")
    print("/!\\ Do not forget to put the bootstrap jumper and to reset your board /!\\")
    print("(be sure to have a file named apf27-u-boot-nand.bin in current dir too)\n")
    print("/!\\ U-Boot recover eat lot of CPU, be sure that no process trust your CPU\n")
    port = input('Enter serial port number or name to use (/dev/ttySx under Linux and COMx under Windows): ')
    try:
        ser = serial.Serial(port, SPEED, timeout=3) 
    except Exception as msg:
        print("unable to open serial port %s" % port)
        print(msg)
        sys.exit()
    ser.flush()

    apfBootloader = apf27Bootloader(ser)
    uboot = UBoot(apfBootloader, ser, UBOOT_IMAGE_BIN)

    apfBootloader.getstatus()
    apfBootloader.initSDRAM1()
    apfBootloader.initSDRAM2()
    uboot.load()
    uboot.waitForPrompt()
    eraseAll = input('Would you like to erase the environment variables? y/n: ')
    if eraseAll == 'y':
        uboot.resetenv()
    uboot.flash()
    print("U-Boot successfully recovered !")