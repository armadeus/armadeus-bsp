# Makefile for Armadeus
#
# Copyright (C) 2005-2016 by the Armadeus Team
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
#

#--------------------------------------------------------------
# Just run 'make menuconfig', configure stuff, then run 'make'.
# You shouldn't need to mess with anything beyond this point...
#--------------------------------------------------------------

ARMADEUS_TOPDIR:=$(shell pwd)
export ARMADEUS_TOPDIR

#--- User configurable stuff:
BUILDROOT_MAJOR_VERSION ?= 2024.02
BUILDROOT_MINOR_VERSION ?= 11
ifneq ($(strip $(BUILDROOT_MINOR_VERSION)),)
BUILDROOT_VERSION := $(BUILDROOT_MAJOR_VERSION).$(BUILDROOT_MINOR_VERSION)
else
BUILDROOT_VERSION := $(BUILDROOT_MAJOR_VERSION)
endif
include ./Makefile.in
BUILDROOT_SITE:=https://buildroot.org/downloads
BUILDROOT_PATCH_DIR:=$(ARMADEUS_TOPDIR)/patches/buildroot/$(BUILDROOT_MAJOR_VERSION)
ARMADEUS_CONFIG_DIR:=$(BUILDROOT_DIR)/configs
#--- End of user conf (don't touch anything below unless you know what you're doing !! ;-) )

BUILDROOT_SOURCE:=buildroot-$(BUILDROOT_VERSION).tar.xz
#BUILDROOT_DIR is defined in ./Makefile.in
PATCH_DIR = patches
BUILDROOT_FILE_PATH = $(ARMADEUS_TOPDIR)/downloads
TAR_OPTIONS = --exclude=.svn --exclude=.git --exclude=.gitignore --strip-component=1 -xf
BUILDROOT_PATCH_SCRIPT = $(BUILDROOT_DIR)/support/scripts/apply-patches.sh
ARMADEUS_ENV_FILE:=$(ARMADEUS_TOPDIR)/armadeus_env.sh
WGET = wget --passive-ftp --tries=3
ARMADEUS_FTP_SITE = ftp://ftp2.armadeus.com/armadeusw/download/

ECHO_CONFIGURATION_NOT_DEFINED:= echo -en "\033[1m"; \
                echo "                                                   "; \
                echo " System not configured. Use make <board>_defconfig " >&2; \
                echo " armadeus valid configurations are:                " >&2; \
                echo " apf27: " >&2; \
                echo "     "$(shell find $(ARMADEUS_CONFIG_DIR) -name "apf27*_defconfig" | sed 's/.*\///');\
                echo " apf51: " >&2; \
                echo "     "$(shell find $(ARMADEUS_CONFIG_DIR) -name "apf51*_defconfig" | sed 's/.*\///');\
                echo " apf28: " >&2; \
                echo "     "$(shell find $(ARMADEUS_CONFIG_DIR) -name "apf28*_defconfig" | sed 's/.*\///');\
                echo " pps: " >&2; \
                echo "     "$(shell find $(ARMADEUS_CONFIG_DIR) -name "pps*_defconfig" | sed 's/.*\///');\
                echo " apf6: " >&2; \
                echo "     "$(shell find $(ARMADEUS_CONFIG_DIR) -name "apf6*_defconfig" | sed 's/.*\///');\
                echo " opos6ul: " >&2; \
                echo "     "$(shell find $(ARMADEUS_CONFIG_DIR) -name "opos6ul*_defconfig" | sed 's/.*\///' | sed 's/opos6ulnano.*/ /g' | sed 's/opos6ulsp.*//g');\
                echo " opos6ulnano: " >&2; \
                echo "     "$(shell find $(ARMADEUS_CONFIG_DIR) -name "opos6ulnano*_defconfig" | sed 's/.*\///');\
                echo " opos6ulsp: " >&2; \
                echo "     "$(shell find $(ARMADEUS_CONFIG_DIR) -name "opos6ulsp*_defconfig" | sed 's/.*\///');\
                echo " opos8mm: " >&2; \
                echo "     "$(shell find $(ARMADEUS_CONFIG_DIR) -name "opos8mm*_defconfig" | sed 's/.*\///');\
                echo " opos93: " >&2; \
                echo "     "$(shell find $(ARMADEUS_CONFIG_DIR) -name "opos93*_defconfig" | sed 's/.*\///' | sed 's/opos93sp.*/ /g');\
                echo " opos93sp: " >&2; \
                echo "     "$(shell find $(ARMADEUS_CONFIG_DIR) -name "opos93*_defconfig" | sed 's/.*\///');\
                echo "                                                   "; \
		echo -en "\033[0m";

ARMADEUS_BSP_VERSION=$(shell cat $(BUILDROOT_DIR)/target/device/armadeus/rootfs/target_overlay/etc/armadeus-version)


default: all

help:
	@echo
	@echo 'First, choose your platform and adjust configuration:'
	@echo '  apf9328_defconfig        - get default config for an APF9328+Dev kit'
	@echo '  apf27_defconfig          - get default config for an APF27+Dev kit'
	@echo '  pps_defconfig            - get default config for an APF27+PPS system'
	@echo '  apf51_defconfig          - get default config for an APF51+Dev kit'
	@echo '  pps51_defconfig          - get default config for an APF51+PPS51 system'
	@echo '  apf28_defconfig          - get default config for an APF28+Dev kit'
	@echo '  apf6_defconfig           - get default config for an APF6+Dev kit (3.19+ mainline/vanilla kernel)'
	@echo '  apf6legacy_defconfig     - get default config for an APF6+Dev kit (3.14 Freescale kernel)'
	@echo '  apf6legacy-4.1_defconfig - get default config for an APF6+Dev kit (4.1 Freescale kernel)'
	@echo '  opos6ul_defconfig        - get default config for an OPOS6UL+Dev kit (4.8+ mainline/vanilla kernel)'
	@echo '  opos6ul_legacy_defconfig - get default config for an OPOS6UL+Dev kit (3.14 Freescale kernel)'
	@echo '  opos6ulnano_defconfig    - get default config for an OPOS6ULNANO+Dev kit (4.9+ mainline/vanilla kernel)'
	@echo '  opos6ulsp_defconfig      - get default config for an OPOS6ULSP+Dev kit (4.19+ mainline/vanilla kernel)'
	@echo '  opos8mm-legacy-5.10_defconfig - get default config for an OPOS8MM+Dev kit (5.10 NXP kernel)'
	@echo '  opos93-legacy-6.1_defconfig   - get default config for an OPOS93+Dev kit (6.1 NXP kernel)'
	@echo '  opos93sp-legacy-6.1_defconfig - get default config for an OPOS93SP+Dev kit (6.1 NXP kernel)'
	@echo ''
	@echo 'Build:'
	@echo '  all                - make world '
	@echo '  <package>          - a single package (ex: uboot, linux, sdl, etc...)'
	@echo
	@echo 'Cleaning:'
	@echo '  clean              - delete files created by build'
	@echo '  distclean          - delete all non-source files (including .config)'
	@echo
	@echo 'Configuration:'
	@echo ' Global:'
	@echo '  menuconfig           - interactive curses-based configurator'
	@echo ' Specific (after first build was successful):'
	@echo '  linux-menuconfig     - configure Linux parameters'
	@echo '  uclibc-menuconfig    - configure uClibc library parameters'
	@echo '  busybox-menuconfig   - configure Busybox parameters'
	@echo
	@echo 'Miscellaneous:'
	@echo '  source                 - download all sources needed for offline-build'
	@echo '  source-check           - check all packages for valid download URLs'
	@echo
	@echo 'See http://www.armadeus.org for further details'
	@echo 'See http://www.buildroot.net/downloads/buildroot.html for'
	@echo 'further Buildroot details'
	@echo


# configuration
# ---------------------------------------------------------------------------

$(BUILDROOT_FILE_PATH)/$(BUILDROOT_SOURCE):
	mkdir -p $(BUILDROOT_FILE_PATH)
	$(WGET) -N -P $(BUILDROOT_FILE_PATH) $(BUILDROOT_SITE)/$(BUILDROOT_SOURCE) \
		|| $(WGET) -N -P $(BUILDROOT_FILE_PATH) $(ARMADEUS_FTP_SITE)/$(BUILDROOT_SOURCE)

buildroot-sources: $(BUILDROOT_FILE_PATH)/$(BUILDROOT_SOURCE)

$(BUILDROOT_DIR)/.unpacked: $(BUILDROOT_FILE_PATH)/$(BUILDROOT_SOURCE) $(BUILDROOT_PATCH_DIR)/*.patch
	$(BUILDROOT_PATCH_DIR)/cleanup_buildroot.sh
	rm -f $(BUILDROOT_DIR)/.applied_patches_list
	xzcat $(BUILDROOT_FILE_PATH)/$(BUILDROOT_SOURCE) | \
		tar -C $(BUILDROOT_DIR) $(TAR_OPTIONS) -
	touch $@

buildroot-unpacked: $(BUILDROOT_DIR)/.unpacked

$(BUILDROOT_DIR)/.patched: $(BUILDROOT_DIR)/.unpacked
	$(BUILDROOT_PATCH_SCRIPT) $(BUILDROOT_DIR) $(BUILDROOT_PATCH_DIR) \*.patch
	touch $@

buildroot-patched: $(BUILDROOT_DIR)/.patched

$(BUILDROOT_OUTPUT_DIR)/.configured: $(BUILDROOT_DIR)/.patched
	@if [ ! -f $@ ]; then \
		$(ECHO_CONFIGURATION_NOT_DEFINED) \
		exit 1; \
	fi

# To be called only one time if one wants to make an automatic build
buildauto: $(BUILDROOT_DIR)/.patched
	# ! Be sure that /local/downloads exists if you want to use automated build !
	BUILDROOT_DL_DIR=/local/downloads BR2_DL_DIR=/local/downloads $(MAKE) -s -C $(BUILDROOT_DIR)

linux: $(BUILDROOT_OUTPUT_DIR)/.configured
	@$(MAKE) -C $(BUILDROOT_DIR) linux-rebuild

linux-clean: $(BUILDROOT_OUTPUT_DIR)/.configured
	@$(MAKE) -C $(BUILDROOT_DIR) linux-clean

%_defconfig: $(BUILDROOT_DIR)/.patched
	@if [ -e "$(ARMADEUS_CONFIG_DIR)/$@" ] || [ -e "$(ARMADEUS_CONFIG_DIR)/$(patsubst %_defconfig,%,$@)/$@" ] || [ -e "$(BR2_EXTERNAL)/configs/$@" ]; then \
		rm -f $(BUILDROOT_CONFIG_DIR)/.config ; \
		$(MAKE) -C $(BUILDROOT_DIR) $@ ; \
		$(MAKE) -C $(BUILDROOT_DIR) menuconfig ; \
		touch $(BUILDROOT_OUTPUT_DIR)/.configured ; \
	else \
		echo -e "\033[1m\nThis configuration does not exist !!\n\033[0m" ; \
	fi;

# for the one who doesn't want config menu:
%_autoconf: $(BUILDROOT_DIR)/.patched
	@if [ -e "$(ARMADEUS_CONFIG_DIR)/$(patsubst %_autoconf,%,$@)_defconfig" ] || [ -e "$(ARMADEUS_CONFIG_DIR)/$(patsubst %_autoconf,%,$@)/$(patsubst %_autoconf,%,$@)_defconfig" ] || [ -e "$(BR2_EXTERNAL)/configs/$(patsubst %_autoconf,%,$@)_defconfig" ]; then \
		rm -f $(BUILDROOT_CONFIG_DIR)/.config ; \
		$(MAKE) -C $(BUILDROOT_DIR) $(patsubst %_autoconf,%,$@)_defconfig ; \
		touch $(BUILDROOT_OUTPUT_DIR)/.configured ; \
	else \
		echo -e "\033[1m\nThis configuration does not exist !!\n\033[0m" ; \
	fi;

all: $(BUILDROOT_OUTPUT_DIR)/.configured
	@mkdir -p $(BUILDROOT_OUTPUT_DIR)
	@echo -n `date +%s` > $(BUILDROOT_OUTPUT_DIR)/before.txt
	@$(MAKE) -C $(BUILDROOT_DIR) $@
	@./scripts/show_build_infos.sh
	@echo -n `date +%s` > $(BUILDROOT_OUTPUT_DIR)/after.txt
	@BEFORE=`cat $(BUILDROOT_OUTPUT_DIR)/before.txt`; AFTER=`cat $(BUILDROOT_OUTPUT_DIR)/after.txt`; BUILD_TIME_IN_SEC=`expr $$AFTER - $$BEFORE`; echo -e "\033[1mBuild time: $$BUILD_TIME_IN_SEC seconds\033[0m"
	@rm $(BUILDROOT_OUTPUT_DIR)/before.txt $(BUILDROOT_OUTPUT_DIR)/after.txt

menuconfig: $(BUILDROOT_OUTPUT_DIR)/.configured
	@$(MAKE) -C $(BUILDROOT_DIR) $@

# !! .DEFAULT do NOT handle dependencies !!
.DEFAULT:
	@if [ ! -e "$(BUILDROOT_OUTPUT_DIR)/.configured" ]; then \
		$(ECHO_CONFIGURATION_NOT_DEFINED) \
		exit 1; \
	fi
	@$(MAKE) -C $(BUILDROOT_DIR) $@

clean:
	@echo "Are you sure you want to delete all generated files (toolchain, kernel & Co) ?"
	@echo " Ctrl+c to stop now"
	@read answer
	rm -rf $(BUILDROOT_OUTPUT_DIR)
	rm -f $(ARMADEUS_ENV_FILE)

buildroot-dirclean:
	rm -rf $(BUILDROOT_DIR)

# Generate shell's most useful variables:
shell_env:
	@echo "# Automatically generated file ! (by top Makefile)" > $(ARMADEUS_ENV_FILE)
	@echo ARMADEUS_BUILDROOT_DIR=$(BUILDROOT_DIR) >> $(ARMADEUS_ENV_FILE)
	@echo ARMADEUS_BUILDROOT_VERSION=$(BUILDROOT_VERSION) >> $(ARMADEUS_ENV_FILE)
	@echo ARMADEUS_BUILDROOT_PATCH_DIR=$(BUILDROOT_PATCH_DIR) >> $(ARMADEUS_ENV_FILE)
	@echo ARMADEUS_LINUX_DIR=$(ARMADEUS_LINUX_DIR) >> $(ARMADEUS_ENV_FILE)
	@echo "ARMADEUS_LINUX_PATCH_DIR=\"$(ARMADEUS_LINUX_PATCH_DIR)\"" >> $(ARMADEUS_ENV_FILE)
	@echo ARMADEUS_LINUX_CONFIG=$(ARMADEUS_LINUX_CONFIG) >> $(ARMADEUS_ENV_FILE)
	@echo ARMADEUS_LINUX_VERSION=$(ARMADEUS_LINUX_VERSION) >> $(ARMADEUS_ENV_FILE)
	@echo ARMADEUS_LINUX_MAIN_VERSION=$(ARMADEUS_LINUX_MAIN_VERSION) >> $(ARMADEUS_ENV_FILE)
	@echo ARMADEUS_U_BOOT_DIR=$(ARMADEUS_U_BOOT_DIR) >> $(ARMADEUS_ENV_FILE)
	@echo "ARMADEUS_U_BOOT_PATCH_DIR=\"$(ARMADEUS_U_BOOT_PATCH_DIR)\"" >> $(ARMADEUS_ENV_FILE)
	@echo ARMADEUS_U_BOOT_VERSION=$(ARMADEUS_U_BOOT_VERSION) >> $(ARMADEUS_ENV_FILE)
	@echo ARMADEUS_U_BOOT_CUSTOM_VERSION=$(ARMADEUS_U_BOOT_CUSTOM_VERSION) >> $(ARMADEUS_ENV_FILE)
	@echo ARMADEUS_U_BOOT_CONFIG=$(ARMADEUS_U_BOOT_CONFIG) >> $(ARMADEUS_ENV_FILE)
	@echo ARMADEUS_ROOTFS_DIR=$(ARMADEUS_ROOTFS_DIR) >> $(ARMADEUS_ENV_FILE)
	@echo ARMADEUS_BINARIES=$(ARMADEUS_BINARIES) >> $(ARMADEUS_ENV_FILE)
	@echo ARMADEUS_ROOTFS_TAR=$(ARMADEUS_ROOTFS_TAR) >> $(ARMADEUS_ENV_FILE)
	@echo ARMADEUS_STAGING_DIR=$(ARMADEUS_STAGING_DIR) >> $(ARMADEUS_ENV_FILE)
	@echo ARMADEUS_TOOLCHAIN_PATH=$(ARMADEUS_TOOLCHAIN_PATH) >> $(ARMADEUS_ENV_FILE)
	@echo ARMADEUS_QT_DIR=$(ARMADEUS_QT_DIR) >> $(ARMADEUS_ENV_FILE)
	@echo ARMADEUS_GCC_VERSION=$(ARMADEUS_GCC_VERSION) >> $(ARMADEUS_ENV_FILE)
	@echo ARMADEUS_LIBC_NAME=$(ARMADEUS_LIBC_NAME) >> $(ARMADEUS_ENV_FILE)
	@echo ARMADEUS_BUSYBOX_VERSION=$(ARMADEUS_BUSYBOX_VERSION) >> $(ARMADEUS_ENV_FILE)
	@echo ARMADEUS_BSP_VERSION=$(ARMADEUS_BSP_VERSION) >> $(ARMADEUS_ENV_FILE)
	@echo ARMADEUS_BOARD_NAME=$(ARMADEUS_BOARD_NAME) >> $(ARMADEUS_ENV_FILE)
	@echo ARMADEUS_U_BOOT_BOARD_NAME=$(ARMADEUS_U_BOOT_BOARD_NAME) >> $(ARMADEUS_ENV_FILE)
	@echo ARMADEUS_DL_DIR=$(ARMADEUS_DL_DIR) >> $(ARMADEUS_ENV_FILE)

PHONY_TARGETS+=dummy all linux linux-clean buildroot-clean buildroot-dirclean
PHONY_TARGETS+=menuconfig $(BUILDROOT_CONFIG_DIR)/.config shell_env buildroot-sources buildroot-unpacked
PHONY_TARGETS+=buildroot-patched 
.PHONY: $(PHONY_TARGETS)

