library IEEE;
 use IEEE.STD_LOGIC_1164.ALL;
 use IEEE.numeric_std.all;
 
 entity led is
    Port ( 
           clk : in  std_logic;
           led : out std_logic
    );
 end led;
 
 architecture RTL of led is
    constant max_count : natural := 48000000;
    signal Rst : std_logic;
    signal gclk : std_logic;

    component pcie_pll
        port (
                refclk : in std_logic;
                rst : in std_logic;
                outclk_0 : out std_logic
             );
    end component pcie_pll;

 begin

    connect_pcie_pll : pcie_pll
    port map (
        refclk => clk,
        rst => Rst,
        outclk_0 => gclk);

    Rst <= '0';
 
    -- 0 to max_count counter
    compteur : process(gclk, Rst)
        variable count : natural range 0 to max_count;
    begin
        if Rst = '1' then
            count := 0;
            led <= '0';
        elsif rising_edge(gclk) then
            if count < max_count/2 then
                count := count + 1;
                led <= '1';
            elsif count < max_count then
                count := count + 1;
                led <= '0';
            else
                count := 0;
                led <= '0';
            end if;
        end if;
    end process compteur; 
 end RTL;
