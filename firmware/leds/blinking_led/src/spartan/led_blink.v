module Clk_div_led (
    input Clk,
    output led_cathode,
    output reg led_anode
);

    localparam max_count = 48000000;

    reg [$clog2(max_count)-1:0] count;
    assign led_cathode = 1'b0;

    always@(posedge Clk) begin
        if(count < max_count/2) begin
            led_anode <= 1'b1;
            count <= count + 1'b1;
        end
        else if(count < max_count) begin
            led_anode <= 1'b0;
            count <= count + 1'b1;
        end
        else begin
            count <= 0;
            led_anode <= 1'b1;
        end
    end

endmodule
