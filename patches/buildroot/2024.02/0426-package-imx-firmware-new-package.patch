From 36f5d0f18999505862745a277afa9d64702aad1d Mon Sep 17 00:00:00 2001
From: =?UTF-8?q?S=C3=A9bastien=20Szymanski?=
 <sebastien.szymanski@armadeus.com>
Date: Fri, 19 Apr 2024 17:38:12 +0200
Subject: [PATCH 1/1] package/imx-firmware: new package
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit

This package provides firmwares for NXP WiFi + Bluetooth chipsets.

Signed-off-by: Sébastien Szymanski <sebastien.szymanski@armadeus.com>
---
 package/Config.in                      |  1 +
 package/imx-firmware/Config.in         | 62 ++++++++++++++++++++++++++
 package/imx-firmware/imx-firmware.hash |  3 ++
 package/imx-firmware/imx-firmware.mk   | 32 +++++++++++++
 4 files changed, 98 insertions(+)
 create mode 100644 package/imx-firmware/Config.in
 create mode 100644 package/imx-firmware/imx-firmware.hash
 create mode 100644 package/imx-firmware/imx-firmware.mk

diff --git a/package/Config.in b/package/Config.in
index 1935077f0f45..3ecceefef4ec 100644
--- a/package/Config.in
+++ b/package/Config.in
@@ -450,6 +450,7 @@ menu "Firmware"
 	source "package/armbian-firmware/Config.in"
 	source "package/b43-firmware/Config.in"
 	source "package/brcmfmac_sdio-firmware-rpi/Config.in"
+	source "package/imx-firmware/Config.in"
 	source "package/linux-firmware/Config.in"
 	source "package/murata-cyw-fw/Config.in"
 	source "package/odroidc2-firmware/Config.in"
diff --git a/package/imx-firmware/Config.in b/package/imx-firmware/Config.in
new file mode 100644
index 000000000000..d2037b270865
--- /dev/null
+++ b/package/imx-firmware/Config.in
@@ -0,0 +1,62 @@
+config BR2_PACKAGE_IMX_FIRMWARE
+	bool "imx-firmware"
+	help
+	  Firmware for NXP Bluetooth / WiFi chipsets based modules
+
+	  https://github.com/nxp-imx/imx-firmware
+
+if BR2_PACKAGE_IMX_FIRMWARE
+
+config BR2_PACKAGE_IMX_FIRMWARE_8801
+	bool "8801 (SD)"
+	help
+	  NXP 8801 module firmware files.
+
+config BR2_PACKAGE_IMX_FIRMWARE_8987
+	bool "8987 (SD)"
+	help
+	  NXP 8987 module firmware files.
+
+config BR2_PACKAGE_IMX_FIRMWARE_8997
+	bool "8997"
+	help
+	  NXP 8997 module firmware files.
+
+choice
+	prompt "8997 WiFi interface" if BR2_PACKAGE_IMX_FIRMWARE_8997
+
+config BR2_PACKAGE_IMX_FIRMWARE_8997_SD
+	bool "SD"
+
+config BR2_PACKAGE_IMX_FIRMWARE_8997_PCIE
+	bool "PCIe"
+
+endchoice
+
+config BR2_PACKAGE_IMX_FIRMWARE_9098
+	bool "9098"
+	help
+	  NXP 9098 module firmware files.
+
+choice
+	prompt "9098 WiFi interface" if BR2_PACKAGE_IMX_FIRMWARE_9098
+
+config BR2_PACKAGE_IMX_FIRMWARE_9098_SD
+	bool "SD"
+
+config BR2_PACKAGE_IMX_FIRMWARE_9098_PCIE
+	bool "PCIe"
+
+endchoice
+
+config BR2_PACKAGE_IMX_FIRMWARE_IW416
+	bool "IW416 (SD)"
+	help
+	  NXP IW416 module firmware files.
+
+config BR2_PACKAGE_IMX_FIRMWARE_IW612
+	bool "IW612 (SD)"
+	help
+	  NXP IW612 module firmware files.
+
+endif
diff --git a/package/imx-firmware/imx-firmware.hash b/package/imx-firmware/imx-firmware.hash
new file mode 100644
index 000000000000..323130d7c690
--- /dev/null
+++ b/package/imx-firmware/imx-firmware.hash
@@ -0,0 +1,3 @@
+# Locally calculated
+sha256  2feba2969db7b6fc86aa0a13a0993ca241e155c23c0024c41316e67924778f5f  imx-firmware-lf-6.6.3_1.0.0.tar.gz
+sha256  3827bf3548bfd9de73bf51fbdd7a8c674dcecfb249cfa1a7cf17bd2dc4e08a8c  LICENSE.txt
diff --git a/package/imx-firmware/imx-firmware.mk b/package/imx-firmware/imx-firmware.mk
new file mode 100644
index 000000000000..8b5c3fb4a5d5
--- /dev/null
+++ b/package/imx-firmware/imx-firmware.mk
@@ -0,0 +1,32 @@
+################################################################################
+#
+# imx-firmware
+#
+################################################################################
+
+IMX_FIRMWARE_VERSION = lf-6.6.3_1.0.0
+IMX_FIRMWARE_SITE = $(call github,nxp-imx,imx-firmware,$(IMX_FIRMWARE_VERSION))
+IMX_FIRMWARE_LICENSE = NXP Software License Agreement
+IMX_FIRMWARE_LICENSE_FILES = LICENSE.txt
+IMX_FIRMWARE_REDISTRIBUTE = NO
+
+IMX_FIRMWARE_FILES_$(BR2_PACKAGE_IMX_FIRMWARE_8801) += FwImage_8801_SD
+IMX_FIRMWARE_FILES_$(BR2_PACKAGE_IMX_FIRMWARE_8987) += FwImage_8987
+IMX_FIRMWARE_FILES_$(BR2_PACKAGE_IMX_FIRMWARE_8997_SD) += FwImage_8997_SD
+IMX_FIRMWARE_FILES_$(BR2_PACKAGE_IMX_FIRMWARE_8997_PCIE) += FwImage_8997
+IMX_FIRMWARE_FILES_$(BR2_PACKAGE_IMX_FIRMWARE_9098_SD) += FwImage_9098_SD
+IMX_FIRMWARE_FILES_$(BR2_PACKAGE_IMX_FIRMWARE_9098_PCIE) += FwImage_9098_PCIE
+IMX_FIRMWARE_FILES_$(BR2_PACKAGE_IMX_FIRMWARE_IW416) += FwImage_IW416_SD
+IMX_FIRMWARE_FILES_$(BR2_PACKAGE_IMX_FIRMWARE_IW612) += FwImage_IW612_SD
+
+define IMX_FIRMWARE_INSTALL_TARGET_CMDS
+	$(INSTALL) -m 0644 -D $(@D)/nxp/wifi_mod_para.conf \
+		$(TARGET_DIR)/lib/firmware/nxp/wifi_mod_para.conf
+	$(INSTALL) -m 0644 -D $(@D)/nxp/mfguart/helper_uart_3000000.bin \
+		$(TARGET_DIR)/lib/firmware/nxp/helper_uart_3000000.bin
+	$(foreach f,$(IMX_FIRMWARE_FILES_y), \
+		$(INSTALL) -m 0644 -D $(@D)/nxp/$(f)/* $(TARGET_DIR)/lib/firmware/nxp/
+	)
+endef
+
+$(eval $(generic-package))
-- 
2.43.2

