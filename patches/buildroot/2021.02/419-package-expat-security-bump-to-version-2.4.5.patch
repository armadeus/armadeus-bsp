From 21c07c0aaa7e43b8f676c564f173a510d345a77a Mon Sep 17 00:00:00 2001
From: Fabrice Fontaine <fontaine.fabrice@gmail.com>
Date: Sat, 19 Feb 2022 13:28:31 +0100
Subject: [PATCH 1/2] package/expat: security bump to version 2.4.5

Security fixes:
 #562  CVE-2022-25235 -- Passing malformed 2- and 3-byte UTF-8
       sequences (e.g. from start tag names) to the XML
       processing application on top of Expat can cause
       arbitrary damage (e.g. code execution) depending
       on how invalid UTF-8 is handled inside the XML
       processor; validation was not their job but Expat's.
       Exploits with code execution are known to exist.
 #561  CVE-2022-25236 -- Passing (one or more) namespace separator
       characters in "xmlns[:prefix]" attribute values
       made Expat send malformed tag names to the XML
       processor on top of Expat which can cause
       arbitrary damage (e.g. code execution) depending
       on such unexpectable cases are handled inside the XML
       processor; validation was not their job but Expat's.
       Exploits with code execution are known to exist.
 #558  CVE-2022-25313 -- Fix stack exhaustion in doctype parsing
       that could be triggered by e.g. a 2 megabytes
       file with a large number of opening braces.
       Expected impact is denial of service or potentially
       arbitrary code execution.
 #560  CVE-2022-25314 -- Fix integer overflow in function copyString;
       only affects the encoding name parameter at parser creation
       time which is often hardcoded (rather than user input),
       takes a value in the gigabytes to trigger, and a 64-bit
       machine.  Expected impact is denial of service.
 #559  CVE-2022-25315 -- Fix integer overflow in function storeRawNames;
       needs input in the gigabytes and a 64-bit machine.
       Expected impact is denial of service or potentially
       arbitrary code execution.

https://blog.hartwork.org/posts/expat-2-4-5-released/
https://github.com/libexpat/libexpat/blob/R_2_4_5/expat/Changes

Signed-off-by: Fabrice Fontaine <fontaine.fabrice@gmail.com>
Signed-off-by: Yann E. MORIN <yann.morin.1998@free.fr>
---
 package/expat/expat.hash | 8 ++++----
 package/expat/expat.mk   | 2 +-
 2 files changed, 5 insertions(+), 5 deletions(-)

diff --git a/package/expat/expat.hash b/package/expat/expat.hash
index 10e13206d0..cd2e8c7fb9 100644
--- a/package/expat/expat.hash
+++ b/package/expat/expat.hash
@@ -1,7 +1,7 @@
-# From https://sourceforge.net/projects/expat/files/expat/2.4.4/
-md5  a712d23b7afb32e7527cf0b3fd8f12ac  expat-2.4.4.tar.xz
-sha1  6028b04d3505fc519c4c7c6bd67e1ad1a08cd7b6  expat-2.4.4.tar.xz
+# From https://sourceforge.net/projects/expat/files/expat/2.4.5/
+md5  9dc183e6c1eebc1b658c1e71be0885e1  expat-2.4.5.tar.xz
+sha1  360a49b55c442e91cb7b328bc0335083909ddf4f  expat-2.4.5.tar.xz
 
 # Locally calculated
-sha256  b5d25d6e373351c2ed19b562b4732d01d2589ac8c8e9e7962d8df1207cc311b8  expat-2.4.4.tar.xz
+sha256  f2af8fc7cdc63a87920da38cd6d12cb113c3c3a3f437495b1b6541e0cff32579  expat-2.4.5.tar.xz
 sha256  8c6b5b6de8fae20b317f4992729abc0e520bfba4c7606cd1e9eeb87418eebdec  COPYING
diff --git a/package/expat/expat.mk b/package/expat/expat.mk
index 250600c25e..7815d61027 100644
--- a/package/expat/expat.mk
+++ b/package/expat/expat.mk
@@ -4,7 +4,7 @@
 #
 ################################################################################
 
-EXPAT_VERSION = 2.4.4
+EXPAT_VERSION = 2.4.5
 EXPAT_SITE = http://downloads.sourceforge.net/project/expat/expat/$(EXPAT_VERSION)
 EXPAT_SOURCE = expat-$(EXPAT_VERSION).tar.xz
 EXPAT_INSTALL_STAGING = YES
-- 
2.34.1

