Index: linux-4.9.59/drivers/tty/serial/mxs-auart.c
===================================================================
--- linux-4.9.59.orig/drivers/tty/serial/mxs-auart.c
+++ linux-4.9.59/drivers/tty/serial/mxs-auart.c
@@ -76,6 +76,7 @@
 
 #define AUART_CTRL1_XFER_COUNT(v)		((v) & 0xffff)
 
+#define AUART_CTRL2_INVERTRTS			(1 << 31)
 #define AUART_CTRL2_DMAONERR			(1 << 26)
 #define AUART_CTRL2_TXDMAE			(1 << 25)
 #define AUART_CTRL2_RXDMAE			(1 << 24)
@@ -427,6 +428,7 @@ struct mxs_auart_port {
 #define MXS_AUART_DMA_TX_SYNC	2  /* bit 2 */
 #define MXS_AUART_DMA_RX_READY	3  /* bit 3 */
 #define MXS_AUART_RTSCTS	4  /* bit 4 */
+#define MXS_AUART_RTS_ACTIVE_H	5  /* bit 5 */
 	unsigned long flags;
 	unsigned int mctrl_prev;
 	enum mxs_auart_type devtype;
@@ -531,6 +533,8 @@ static void mxs_auart_stop_tx(struct uar
 
 static void mxs_auart_tx_chars(struct mxs_auart_port *s);
 
+static unsigned int mxs_auart_tx_empty(struct uart_port *u);
+
 static void dma_tx_callback(void *param)
 {
 	struct mxs_auart_port *s = param;
@@ -583,6 +587,20 @@ static int mxs_auart_dma_tx(struct mxs_a
 	return 0;
 }
 
+static void inline mxs_auart_rts_inactive(struct mxs_auart_port *s)
+{
+	u32 ctrl = mxs_read(s, REG_CTRL2);
+	ctrl &= ~(AUART_CTRL2_RTS);
+	mxs_write(ctrl, s, REG_CTRL2);
+}
+
+static void inline mxs_auart_rts_active(struct mxs_auart_port *s)
+{
+	u32 ctrl = mxs_read(s, REG_CTRL2);
+	ctrl |= AUART_CTRL2_RTS;
+	mxs_write(ctrl, s, REG_CTRL2);
+}
+
 static void mxs_auart_tx_chars(struct mxs_auart_port *s)
 {
 	struct circ_buf *xmit = &s->port.state->xmit;
@@ -614,6 +632,11 @@ static void mxs_auart_tx_chars(struct mx
 		if (i) {
 			mxs_auart_dma_tx(s, i);
 		} else {
+			if (s->port.rs485.flags & SER_RS485_ENABLED) {
+				if (s->port.rs485.delay_rts_after_send)
+					udelay(s->port.rs485.delay_rts_after_send);
+				mxs_auart_rts_inactive(s);
+			}
 			clear_bit(MXS_AUART_DMA_TX_SYNC, &s->flags);
 			smp_mb__after_atomic();
 		}
@@ -643,6 +666,21 @@ static void mxs_auart_tx_chars(struct mx
 	else
 		mxs_set(AUART_INTR_TXIEN, s, REG_INTR);
 
+	/* Last transmission in RS485 mode: */
+	if ((s->port.rs485.flags & SER_RS485_ENABLED) &&
+		(uart_circ_empty(&(s->port.state->xmit)))) {
+		while(!(mxs_auart_tx_empty(&s->port))) {
+			/* Just wait for TX FIFO empty state */
+		}
+		if (s->port.rs485.delay_rts_during_last_tx)
+			/* Even if FIFO is empty, we still need to wait the transmit of last char */
+			udelay(s->port.rs485.delay_rts_during_last_tx);
+		if (s->port.rs485.delay_rts_after_send)
+			/* User configured extra rts delay after the transmitted packet */
+			udelay(s->port.rs485.delay_rts_after_send);
+		mxs_auart_rts_inactive(s);
+	}
+
 	if (uart_tx_stopped(&s->port))
 		mxs_auart_stop_tx(&s->port);
 }
@@ -739,19 +777,21 @@ static void mxs_auart_set_mctrl(struct u
 {
 	struct mxs_auart_port *s = to_auart_port(u);
 
-	u32 ctrl = mxs_read(s, REG_CTRL2);
+	if ((s->port.rs485.flags & SER_RS485_ENABLED) == 0) {
+		u32 ctrl = mxs_read(s, REG_CTRL2);
 
-	ctrl &= ~(AUART_CTRL2_RTSEN | AUART_CTRL2_RTS);
-	if (mctrl & TIOCM_RTS) {
-		if (uart_cts_enabled(u))
-			ctrl |= AUART_CTRL2_RTSEN;
-		else
-			ctrl |= AUART_CTRL2_RTS;
-	}
+		ctrl &= ~(AUART_CTRL2_RTSEN | AUART_CTRL2_RTS);
+		if (mctrl & TIOCM_RTS) {
+			if (uart_cts_enabled(u))
+				ctrl |= AUART_CTRL2_RTSEN;
+			else
+				ctrl |= AUART_CTRL2_RTS;
+		}
 
-	mxs_write(ctrl, s, REG_CTRL2);
+		mxs_write(ctrl, s, REG_CTRL2);
 
-	mctrl_gpio_set(s->gpios, mctrl);
+		mctrl_gpio_set(s->gpios, mctrl);
+	}
 }
 
 #define MCTRL_ANY_DELTA        (TIOCM_RI | TIOCM_DSR | TIOCM_CD | TIOCM_CTS)
@@ -1053,7 +1093,9 @@ static void mxs_auart_settermios(struct
 
 	/* figure out the hardware flow control settings */
 	ctrl2 &= ~(AUART_CTRL2_CTSEN | AUART_CTRL2_RTSEN);
-	if (cflag & CRTSCTS) {
+	if (test_bit(MXS_AUART_RTS_ACTIVE_H, &s->flags))
+		ctrl2 |= AUART_CTRL2_INVERTRTS;
+	if ((cflag & CRTSCTS) && ((s->port.rs485.flags & SER_RS485_ENABLED) == 0)) {
 		/*
 		 * The DMA has a bug(see errata:2836) in mx23.
 		 * So we can not implement the DMA for auart in mx23,
@@ -1092,6 +1134,9 @@ static void mxs_auart_settermios(struct
 	ctrl |= AUART_LINECTRL_BAUD_DIVINT(div >> 6);
 	mxs_write(ctrl, s, REG_LINECTRL);
 
+	/* set RS-485 last char Tx delay in usec - worst case 12 bits */
+	s->port.rs485.delay_rts_during_last_tx = 12000000 / baud;
+
 	mxs_write(ctrl2, s, REG_CTRL2);
 
 	uart_update_timeout(u, termios->c_cflag, baud);
@@ -1287,6 +1332,12 @@ static void mxs_auart_start_tx(struct ua
 {
 	struct mxs_auart_port *s = to_auart_port(u);
 
+	if (s->port.rs485.flags & SER_RS485_ENABLED) {
+		mxs_auart_rts_active(s);
+		if (s->port.rs485.delay_rts_before_send)
+			udelay(s->port.rs485.delay_rts_before_send);
+	}
+
 	/* enable transmitter */
 	mxs_set(AUART_CTRL2_TXE, s, REG_CTRL2);
 
@@ -1317,6 +1368,28 @@ static void mxs_auart_break_ctl(struct u
 		mxs_clr(AUART_LINECTRL_BRK, s, REG_LINECTRL);
 }
 
+static int mxs_auart_rs485_config(struct uart_port *u,
+				struct serial_rs485 *rs485conf)
+{
+	struct mxs_auart_port *s = to_auart_port(u);
+	struct device *dev = u->dev;
+
+	/* RTS is required to control the transmitter */
+	if (!test_bit(MXS_AUART_RTSCTS, &s->flags)) {
+		rs485conf->flags &= ~SER_RS485_ENABLED;
+		dev_err(dev, "RTS is required to activate RS-485 support !");
+	}
+
+	if (rs485conf->flags & SER_RS485_ENABLED) {
+		/* disable transmitter */
+		mxs_auart_rts_inactive(s);
+	}
+
+	u->rs485 = *rs485conf;
+
+	return 0;
+}
+
 static const struct uart_ops mxs_auart_ops = {
 	.tx_empty       = mxs_auart_tx_empty,
 	.start_tx       = mxs_auart_start_tx,
@@ -1575,6 +1648,9 @@ static int serial_mxs_probe_dt(struct mx
 	    of_get_property(np, "fsl,uart-has-rtscts", NULL) /* deprecated */)
 		set_bit(MXS_AUART_RTSCTS, &s->flags);
 
+	if (of_get_property(np, "rs485-rts-active-high", NULL))
+		set_bit(MXS_AUART_RTS_ACTIVE_H, &s->flags);
+
 	return 0;
 }
 
@@ -1685,6 +1761,8 @@ static int mxs_auart_probe(struct platfo
 	s->port.fifosize = MXS_AUART_FIFO_SIZE;
 	s->port.uartclk = clk_get_rate(s->clk);
 	s->port.type = PORT_IMX;
+	s->port.rs485_config = mxs_auart_rs485_config;
+	s->port.rs485.flags = SER_RS485_RTS_ON_SEND;
 
 	mxs_init_regs(s);
 
Index: linux-4.9.59/include/uapi/linux/serial.h
===================================================================
--- linux-4.9.59.orig/include/uapi/linux/serial.h
+++ linux-4.9.59/include/uapi/linux/serial.h
@@ -124,7 +124,8 @@ struct serial_rs485 {
 #define SER_RS485_RX_DURING_TX		(1 << 4)
 	__u32	delay_rts_before_send;	/* Delay before send (milliseconds) */
 	__u32	delay_rts_after_send;	/* Delay after send (milliseconds) */
-	__u32	padding[5];		/* Memory is cheap, new structs
+	__u32	delay_rts_during_last_tx;	/* Delay during last FIFO element tx (milliseconds) */
+	__u32	padding[4];		/* Memory is cheap, new structs
 					   are a royal PITA .. */
 };
 
