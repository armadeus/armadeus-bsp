If a PHY uses ENET_OUT as reference clock, it may need a RESET to get
functional after the clock had been disabled.

Failure to do this results in the link state constantly toggling
between up and down:
fec 02188000.ethernet eth0: Link is Up - 100Mbps/Full - flow control rx/tx
fec 02188000.ethernet eth0: Link is Down
fec 02188000.ethernet eth0: Link is Up - 100Mbps/Full - flow control rx/tx
fec 02188000.ethernet eth0: Link is Down
[...]

Signed-off-by: Lothar Waßmann <LW@KARO-electronics.de>

Index: linux-3.19.8/drivers/net/ethernet/freescale/fec.h
===================================================================
--- linux-3.19.8.orig/drivers/net/ethernet/freescale/fec.h
+++ linux-3.19.8/drivers/net/ethernet/freescale/fec.h
@@ -514,6 +514,7 @@ struct fec_enet_private {
 	bool	bufdesc_ex;
 	int	pause_flag;
 	u32	quirks;
+	int	phy_reset_gpio;
 
 	struct	napi_struct napi;
 	int	csum_flags;
Index: linux-3.19.8/drivers/net/ethernet/freescale/fec_main.c
===================================================================
--- linux-3.19.8.orig/drivers/net/ethernet/freescale/fec_main.c
+++ linux-3.19.8/drivers/net/ethernet/freescale/fec_main.c
@@ -65,6 +65,7 @@
 
 static void set_multicast_list(struct net_device *ndev);
 static void fec_enet_itr_coal_init(struct net_device *ndev);
+static void fec_reset_phy(struct platform_device *pdev);
 
 #define DRIVER_NAME	"fec"
 
@@ -1807,6 +1808,8 @@ static int fec_enet_clk_enable(struct ne
 			ret = clk_prepare_enable(fep->clk_enet_out);
 			if (ret)
 				goto failed_clk_enet_out;
+
+			fec_reset_phy(fep->pdev);
 		}
 		if (fep->clk_ptp) {
 			mutex_lock(&fep->ptp_clk_mutex);
@@ -3026,11 +3029,12 @@ static int fec_enet_init(struct net_devi
 #ifdef CONFIG_OF
 static void fec_reset_phy(struct platform_device *pdev)
 {
-	int err, phy_reset;
-	int msec = 1;
+	struct net_device *ndev = platform_get_drvdata(pdev);
+	struct fec_enet_private *fep = netdev_priv(ndev);
 	struct device_node *np = pdev->dev.of_node;
+	int msec = 1;
 
-	if (!np)
+	if (!gpio_is_valid(fep->phy_reset_gpio))
 		return;
 
 	of_property_read_u32(np, "phy-reset-duration", &msec);
@@ -3038,18 +3042,27 @@ static void fec_reset_phy(struct platfor
 	if (msec > 1000)
 		msec = 1;
 
+	gpio_set_value_cansleep(fep->phy_reset_gpio, 0);
+	msleep(msec);
+	gpio_set_value_cansleep(fep->phy_reset_gpio, 1);
+}
+
+static int fec_get_reset_gpio(struct platform_device *pdev)
+{
+	int err, phy_reset;
+	struct device_node *np = pdev->dev.of_node;
+
 	phy_reset = of_get_named_gpio(np, "phy-reset-gpios", 0);
 	if (!gpio_is_valid(phy_reset))
-		return;
+		return phy_reset;
 
 	err = devm_gpio_request_one(&pdev->dev, phy_reset,
 				    GPIOF_OUT_INIT_LOW, "phy-reset");
 	if (err) {
 		dev_err(&pdev->dev, "failed to get phy-reset-gpios: %d\n", err);
-		return;
+		return err;
 	}
-	msleep(msec);
-	gpio_set_value(phy_reset, 1);
+	return phy_reset;
 }
 #else /* CONFIG_OF */
 static void fec_reset_phy(struct platform_device *pdev)
@@ -3059,6 +3072,11 @@ static void fec_reset_phy(struct platfor
 	 * by machine code.
 	 */
 }
+
+static inline int fec_get_reset_gpio(struct platform_device *pdev)
+{
+	return -EINVAL;
+}
 #endif /* CONFIG_OF */
 
 static void
@@ -3154,6 +3172,11 @@ fec_probe(struct platform_device *pdev)
 
 	platform_set_drvdata(pdev, ndev);
 
+	ret = fec_get_reset_gpio(pdev);
+	if (ret == -EPROBE_DEFER)
+		goto gpio_defer;
+	fep->phy_reset_gpio = ret;
+
 	phy_node = of_parse_phandle(np, "phy-handle", 0);
 	if (!phy_node && of_phy_is_fixed_link(np)) {
 		ret = of_phy_register_fixed_link(np);
@@ -3283,6 +3306,7 @@ failed_regulator:
 failed_clk:
 failed_phy:
 	of_node_put(phy_node);
+gpio_defer:
 failed_ioremap:
 	free_netdev(ndev);
 
