/*
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
** 
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software 
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*/ 

#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#include <linux/i2c.h> 
#include <linux/i2c-dev.h> 
#include <sys/ioctl.h>

#define DEBUG_OV5640	1

#define VERSION "0.1"
#define CONF_FILE "ov5640.conf"
#define MAX_DUMP_LINE 100

#define OV5640_I2C_SLAVE_ADDR	0x3c

#define CHIP_REVISION	0x302a

void usage(char *name)
{
	printf("\n%s utility, version %s \n", name, VERSION);
	printf("q: quit\n");
	printf("r: read a register\n");
	printf("w: write to a register\n");
	printf("u: move ROI up\n");
	printf("h: move ROI left (from sensor PoW)\n");
	printf("j: move ROI right (from sensor PoW)\n");
	printf("n: move ROI down\n");
	printf("d: dump some OV5640 registers\n");
	printf("s: save\n");
	printf("l: load\n");
	printf("other: this menu\n");
}

int read_reg(int i2c_fd, unsigned int reg, unsigned char *rbuf)
{
	unsigned char buf[2];
	int ret;

	buf[0] = reg >> 8;
	buf[1] = reg & 0xff;

	ret = write(i2c_fd, buf, 2);
        if (ret < 0) {
                perror("read_reg, setting reg addr");
		return ret;
        }
	read(i2c_fd, rbuf, 1);

	fprintf(stdout, "read 0x%02x @ 0x%02x%02x\n", rbuf[0], buf[0], buf[1]);

	return 0;
}

int write_reg(int i2c_fd, unsigned int reg, unsigned char value)
{
	int ret = 0;
	int len = 3;

	unsigned char buf[3];

	buf[0] = reg >> 8;
	buf[1] = reg & 0xff;
	buf[2] = value;

	fprintf(stdout, "writing 0x%02x @ 0x%02x%02x\n", value, buf[0], buf[1]);

	ret = write(i2c_fd, buf, len);
        if (ret < 0) {
                fprintf(stderr, "Error: Write failed\n");
		return ret;
        }

	return ret;
}

void dump(int fd, FILE* destFile)
{
	int i;
	unsigned char buf[2];

	for (i = 0; i < 16; i++) {
		read_reg(fd, 0x3800 + i, buf);
	}
}

void move_roi(int fd, int x_off, int y_off)
{
	static unsigned int x_addr_st = 0x150;
	static unsigned int x_add_end = 0x8ef;
	static unsigned int y_addr_st = 0x1b2;
	static unsigned int y_add_end = 0x5f1;

	if (!x_off)
		goto process_y_off;

	if (x_addr_st >= abs(x_off)) {
		x_addr_st += x_off;
		x_add_end += x_off;
	} else {
		fprintf(stdout, "you reached limit of Xoff\n");
		goto no_write_reg;
	}

	write_reg(fd, 0x3800, x_addr_st >> 8);
	write_reg(fd, 0x3801, x_addr_st & 0xff);
	write_reg(fd, 0x3804, x_add_end >> 8);
	write_reg(fd, 0x3805, x_add_end & 0xff);

process_y_off:

	if (y_addr_st >= abs(y_off)) {
		y_addr_st += y_off;
		y_add_end += y_off;
	} else {
		fprintf(stdout, "you reached limit of Yoff\n");
		goto no_write_reg;
	}

	write_reg(fd, 0x3802, y_addr_st >> 8);
	write_reg(fd, 0x3803, y_addr_st & 0xff);
	write_reg(fd, 0x3806, y_add_end >> 8);
	write_reg(fd, 0x3807, y_add_end & 0xff);

no_write_reg:
	return;
}

void write_regs_from_file(int fd, char* fileName)
{
	char line[MAX_DUMP_LINE];
	char tempFileName[MAX_DUMP_LINE];
	FILE *fd_conf = NULL;
	int addr, val, nb = 0;

	if (strlen(fileName) == 0) {
		printf("Enter file name: ");
		fgets(tempFileName, sizeof(tempFileName), stdin);
		tempFileName[strlen(tempFileName)-1] = '\0'; /* suppress \n */
	} else {
		strcpy(tempFileName, fileName);
	}
 
	if ((fd_conf = fopen(tempFileName, "r")) == NULL) {
		perror("can't open conf file");
		return;
	}

	while (fgets(line, MAX_DUMP_LINE, fd_conf) != NULL) {
		nb = sscanf(line, "%4X%*c%2X", &addr, &val);

		fprintf(stdout, "--- %04x %02x (%d)\n", addr, val, nb);
	}

	if (fd_conf != NULL)
		fclose(fd_conf);
}

/* taken from i2c-tools : */
int set_slave_addr(int file, int address, int force)
{
	/* With forcei=1, let the user read from/write to the registers
	  even when a driver is also running */
	if (ioctl(file, force ? I2C_SLAVE_FORCE : I2C_SLAVE, address) < 0) {
		fprintf(stderr,
			"Error: Could not set address to 0x%02x: %s\n",
			address, strerror(errno));
		return -errno;
	}

	return 0;
}

int main(int argc, char *argv[])
{
	int i2c_fd;
	FILE *fd_conf = NULL;
	char string[10];
	int regAddr;
	int regValue;
	char* bus = "/dev/i2c-0";
	unsigned char read_buf[2];

	if (argc > 2) { /* too many args */
		usage(argv[0]);
		exit(1);
	} else if (argc == 2) { /* i2c bus */
		bus = argv[1];
	}

	if ((i2c_fd = open(bus, O_RDWR)) < 0) {
		perror("Open error: ");
		exit (1);
	}

	set_slave_addr(i2c_fd, OV5640_I2C_SLAVE_ADDR, 1);

	/* check OV5640 presence */
	if (!read_reg(i2c_fd, CHIP_REVISION, read_buf)) {
		if (read_buf[0] != 0xb0 ) {
			printf("OV5640 revision (0x%02x) not supported... Exiting\n", read_buf[0]);
			exit(1);
		}
	} else {
		printf("OV5640 not found @ 0x%02X... Exiting\n", OV5640_I2C_SLAVE_ADDR);
		exit(1);
	}
   
	usage(argv[0]);
	while(1) {
		fprintf(stdout, " > ");

		fgets(string, sizeof(string), stdin);

		if (string[0] == 'q') {
			exit(0);
		}
		else if (string[0] == 'w') {
			printf("register address (hex without 0x): ");
			fgets(string, sizeof(string), stdin);
			sscanf(string, "%4x", &regAddr);
			printf("register value (hex without 0x): ");
			fgets(string, sizeof(string), stdin);
			sscanf(string, "%2x", &regValue);
			write_reg(i2c_fd, regAddr, regValue);
		}
		else if (string[0] == 'r') {
			printf("register address (hex without 0x): ");
			fgets(string, sizeof(string), stdin);
			sscanf(string, "%4x", &regAddr);
			read_reg(i2c_fd, regAddr, read_buf);
		}
		else if (string[0] == 'd') {
			dump(i2c_fd, fd_conf);
		}
		else if (string[0] == 'u') {
			move_roi(i2c_fd, 0, -30);
		}
		else if (string[0] == 'j') {
			move_roi(i2c_fd, -30, 0);
		}
		else if (string[0] == 'h') {
			move_roi(i2c_fd, 30, 0);
		}
		else if (string[0] == 'n') {
			move_roi(i2c_fd, 0, 30);
		}
		else if (string[0] == 's') {
			if ((fd_conf = fopen(CONF_FILE, "w")) < 0) {
				perror("Open error: ");
				exit (1);
			}
			/*dump(fd, fd_conf);*/
			if (fd_conf != NULL) {
				fclose(fd_conf);
			}
		}
		else if (string[0] == 'l') {
			write_regs_from_file(i2c_fd, "");
		} else {
			usage(argv[0]);
		}
	};
    
	if (i2c_fd >= 0) {
		close(i2c_fd);
	}
  
	exit (0);
}
