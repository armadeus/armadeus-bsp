#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# License: GNU LESSER GENERAL PUBLIC LICENSE, see COPYING.
#

__doc__ = "This class drives HMC5883L 3-axis digital compass"
__version__ = "0.1.0"
__versionTime__ = "06/10/2016"
__author__ = "Julien Boibessot <julien.boibessot@armadeus.com>"

import smbus
import time

HMC_I2C_ADDR = 0x1e

class HMC5883L(object):
    def __init__(self, bus_id, address):
        self._bus = smbus.SMBus(bus_id)
        if address is not None:
            self._address = address
        else:
            self._address = HMC_I2C_ADDR
        self._get_id()
#        self._dump_regs()

    def _read_reg(self, id):
        self._bus.write_byte(self._address, id)
        reg = self._bus.read_byte(self._address)
        return reg

    def _get_id(self):
        self._bus.write_byte(self._address, 10)
        id_a = self._read_reg(10)
        id_b = self._read_reg(11)
        id_c = self._read_reg(12)
        if id_a != 0x48 or id_b != 0x34 or id_c != 0x33:
            print "No valid HMC5883L IDs found ! (0x%02x 0x%02x 0x%02x != 0x48 0x34 0x33)" % (id_a, id_b, id_c)
        return id

    def _dump_regs(self):
        for i in range(0,13):
            self._bus.write_byte(self._address, i)
            reg = self._bus.read_byte(self._address)
            print "reg%d = 0x%02x" % (i, reg)

    def _twos_comp(self, val, bits):
        """compute the 2's compliment of int value val"""
        if (val & (1 << (bits - 1))) != 0: # if sign bit is set e.g., 8bit: 128-255
            val = val - (1 << bits)        # compute negative value
        return val

    def _be_conv(self, le):
        be = ((le << 8) & 0xFF00) + (le >> 8)
        return be

    def _get_unsigned_calib_coeff(self, reg_addr):
        result = self._bus.read_word_data(self._address, reg_addr)
        result = self._be_conv(result)
        return result

    def _get_signed_calib_coeff(self, reg_addr):
        result = self._get_unsigned_calib_coeff(reg_addr)
        if result > 32767:
            result -= 65536
        return result

    def start_single_measurement(self):
        data = [0, 0, 0]
	self._bus.write_byte_data(self._address, 0, 0x70)                      
	self._bus.write_byte_data(self._address, 1, 0xe0)
	self._bus.write_byte_data(self._address, 2, 0x01)
        time.sleep(0.006)	# 6ms
#        reg = self._bus.read_block_data(self._address, 6)
        for i in (0, 2, 4):
            MSB = self._read_reg(3+i)
            LSB = self._read_reg(4+i)
            print "MSB = 0x%02x LSB = 0x%02x" % (MSB, LSB)
            val = (MSB << 8) + LSB
            val = self._twos_comp(val, 16)
            data[i/2] = val
        print data
         
# For test purpose:
hmc = HMC5883L(bus_id=1, address=0x1e)
hmc.start_single_measurement()
