#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# License: GNU LESSER GENERAL PUBLIC LICENSE, see COPYING.
#

__doc__ = "This class drives BMP180 digital pressure sensor"
__version__ = "0.1.0"
__versionTime__ = "06/10/2016"
__author__ = "Julien Boibessot <julien.boibessot@armadeus.com>"

import smbus
import time

BMP_I2C_ADDR = 0x77

class BMP180(object):
    def __init__(self, bus_id, address):
        self._bus = smbus.SMBus(bus_id)
        if address is not None:
            self._address = address
        else:
            self._address = BMP_I2C_ADDR
        self._get_id()
        self._load_calibration_coeffs()
        self.display_calibration_coeffs()

    def _get_id(self):
        id = self._bus.read_byte_data(self._address, 0xd0)
        if id != 0x55:
            print "No valid BMP180 ID found ! (0x%02x != 0x55)" % id
        return id

    def _be_conv(self, le):
        """little endian -> big endian"""
        be = ((le << 8) & 0xFF00) + (le >> 8)
        return be

    def _get_unsigned_calib_coeff(self, reg_addr):
        result = self._bus.read_word_data(self._address, reg_addr)
        result = self._be_conv(result)
        return result

    def _get_signed_calib_coeff(self, reg_addr):
        result = self._get_unsigned_calib_coeff(reg_addr)
        if result > 32767:
            result -= 65536
        return result

    def _load_calibration_coeffs(self):
        self._AC1 = self._get_signed_calib_coeff(0xaa)
        self._AC2 = self._get_signed_calib_coeff(0xac)
        self._AC3 = self._get_signed_calib_coeff(0xae)
        self._AC4 = self._get_unsigned_calib_coeff(0xb0)
        self._AC5 = self._get_unsigned_calib_coeff(0xb2)
        self._AC6 = self._get_unsigned_calib_coeff(0xb4)
        self._B1 = self._get_signed_calib_coeff(0xb6)
        self._B2 = self._get_signed_calib_coeff(0xb8)
        self._MB = self._get_signed_calib_coeff(0xba)
        self._MC = self._get_signed_calib_coeff(0xbc)
        self._MD = self._get_signed_calib_coeff(0xbe)

    def _load_datasheet_calibration_coeffs(self):
        self._AC1 = 408                                           
        self._AC2 = -72
        self._AC3 = -14383
        self._AC4 = 32741                                         
        self._AC5 = 32757
        self._AC6 = 23153
        self._B1 = 6190                                           
        self._B2 = 4
        self._MB = -32767
        self._MC = -8711                                          
        self._MD = 2868

    def display_calibration_coeffs(self):
        """Printout calibration coefficients of chips. Mainly
        for debug purpose."""
        print "AC1 = %d" % self._AC1
        print "AC2 = %d" % self._AC2
        print "AC3 = %d" % self._AC3
        print "AC4 = %d" % self._AC4
        print "AC5 = %d" % self._AC5
        print "AC6 = %d" % self._AC6
        print "B1 = %d" % self._B1
        print "B2 = %d" % self._B2
        print "MB = %d" % self._MB
        print "MC = %d" % self._MC
        print "MD = %d" % self._MD
 
    def get_temperature(self):
        """Return temperature measured by BMP180 in Celsius degrees"""
        # read uncompensated temperature value:
        self._bus.write_byte_data(self._address, 0xf4, 0x2e)
        time.sleep(0.005)
        MSB = self._bus.read_byte_data(self._address, 0xf6)
        LSB = self._bus.read_byte_data(self._address, 0xf7)
        UT = (MSB << 8) + LSB
        #
#        UT = 27898
        #
        # calculate true temperature:
        X1 = (UT-self._AC6)*(self._AC5 / 32768.0)
        X2 = (self._MC * 2048.0) / (X1 + self._MD)
        self._B5 = int(X1) + int(X2)
        print "UT = %d\nX1 = %d\nX2 = %d\nB5 = %d" % (UT, X1, X2, self._B5)
        T = (self._B5 + 8) / 16
        T = T / 10.0
        return T

    def get_pressure(self, oss=0):
        """Return pressure measured by BMP180 in HPa"""
        self.get_temperature() # -> B5
        # read uncompensated pressure value:
        self._bus.write_byte_data(self._address, 0xf4, (0x34+(oss<<6)))
        time.sleep(0.03)	# 30ms, should depend on mode
        MSB = self._bus.read_byte_data(self._address, 0xf6)
        LSB = self._bus.read_byte_data(self._address, 0xf7)
        XLSB = self._bus.read_byte_data(self._address, 0xf8)
        UP = ((MSB << 16) + (LSB << 8) + XLSB) >> (8 - oss)
        #
#        self._load_datasheet_calibration_coeffs()
#        self._B5 = 2399
#        UP = 23843
#        oss = 0
        #
        print "UP = %d" % UP
        # calculate true pressure:
        B6 = self._B5 - 4000
        X1 = int(self._B2 * (B6 * B6 >> 12)) >> 11
        X2 = (self._AC2 * B6) >> 11
        X3 = X1 + X2
        B3 = ((int(self._AC1 * 4 + X3) << oss) + 2) >> 2
        print "B6 = %d\nX1 = %d\nX2 = %d\nX3 = %d\nB3 = %d" % (B6, X1, X2, X3, B3)
        X1 = self._AC3 * B6 / 8192
        X2 = (self._B1 * ((B6 * B6 >> 12))) >> 16
        X3 = ((X1+X2)+2)/4
        B4 = self._AC4 * (X3+32768) >> 15
        B7 = (UP-B3)*(50000>>oss)
        print "X1 = %d\nX2 = %d\nX3 = %d\nB4 = %d\nB7 = %d" % (X1, X2, X3, B4, B7)
        if B7 < 0x80000000:
            p = (B7 * 2) / B4
        else:
            p = (B7 / B4) * 2
        X1 = (p >> 8) * (p >> 8)
        print "X1 = %d" % X1
        X1 = (X1*3038) >> 16
        X2 = (-7357*p) >> 16
        print "X1 = %d\nX2 = %d" % (X1, X2)
        p = p+((X1+X2+3791) >> 4)
        p = p / 100.0
        return p

# For test purpose:
bmp = BMP180(bus_id=1, address=0x77)
temp = bmp.get_temperature()
print ">>> Temperature = %02.02f oC" % temp
pressure = bmp.get_pressure(3)
print ">>> Pressure = %04.02f HPa" % pressure
