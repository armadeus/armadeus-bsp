/*
 * imx6ulregs.h - i.MX6UL registers definition
 *
 * Copyright (C) 2016 Armadeus Systems
 * Authors: Sébastien Szymanski, Julien Boibessot
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#include "definitions.h"

#define IMX_TYPE "i.MX6UL"

static struct reg_info regs[] =
{
{ "CCM_CBCMR",              0x020c4018, 0, 0xffffffff, 'x', "CCM Bus Clock Multiplexer Register" },
{ "CCM_CSCDR2",             0x020c4038, 0, 0xffffffff, 'x', "CCM Serial Clock Divider Register 2" },
{ "CCM_ANALOG_PLL_VIDEO0",  0x020c80a0, 0, 0xffffffff, 'x', "CCM Analog Video PLL Control Register" },
/**/
{ "TEMPMON_TEMPSENSE0",     0x020c8180, 0, 0xffffffff, 'x', "Tempsensor Control Register 0" },
{ "TEMPMON_TEMPSENSE0_SET", 0x020c8184, 0, 0xffffffff, 'x', "Tempsensor Control Register 0" },
{ "TEMPMON_TEMPSENSE0_CLR", 0x020c8188, 0, 0xffffffff, 'x', "Tempsensor Control Register 0" },
{ "TEMPMON_TEMPSENSE0_TOG", 0x020c818C, 0, 0xffffffff, 'x', "Tempsensor Control Register 0" },
{ "TEMPMON_TEMPSENSE1",     0x020c8190, 0, 0xffffffff, 'x', "Tempsensor Control Register 1" },
{ "TEMPMON_TEMPSENSE1_SET", 0x020c8194, 0, 0xffffffff, 'x', "Tempsensor Control Register 1" },
{ "TEMPMON_TEMPSENSE1_CLR", 0x020c8198, 0, 0xffffffff, 'x', "Tempsensor Control Register 1" },
{ "TEMPMON_TEMPSENSE1_TOG", 0x020c819C, 0, 0xffffffff, 'x', "Tempsensor Control Register 1" },
{ "TEMPMON_TEMPSENSE2",     0x020c8290, 0, 0xffffffff, 'x', "Tempsensor Control Register 2" },
{ "TEMPMON_TEMPSENSE2_SET", 0x020c8294, 0, 0xffffffff, 'x', "Tempsensor Control Register 2" },
{ "TEMPMON_TEMPSENSE2_CLR", 0x020c8298, 0, 0xffffffff, 'x', "Tempsensor Control Register 2" },
{ "TEMPMON_TEMPSENSE2_TOG", 0x020c829c, 0, 0xffffffff, 'x', "Tempsensor Control Register 2" },
/**/
{ "LCDIF_CTRL0",            0x021c8000, 0, 0xffffffff, 'x', "eLCDIF General Control Register 0" },
{ "LCDIF_TRANSFER_COUNT",   0x021c8030, 0, 0xffffffff, 'x', "eLCDIF Horizontal and Vertical Count Register" },
{ "LCDIF_CUR_BUF",          0x021c8040, 0, 0xffffffff, 'x', "eLCDIF Current Buffer Address Register" },
{ "LCDIF_NEXT_BUF",         0x021c8050, 0, 0xffffffff, 'x', "eLCDIF Next Buffer Address Register" },
{ "LCDIF_VERSION",          0x021c81c0, 0, 0xffffffff, 'x', "eLCDIF Interface Version Register" },
/*SAI*/
{ "I2S1_TCSR", 		0x02028000, 0, 0xffffffff, 'x', "SAI Transmit Control Register"},
{ "I2S1_TCR1", 		0x02028004, 0, 0xffffffff, 'x', "SAI Transmit Configuration 1 Register"},
{ "I2S1_TCR2", 		0x02028008, 0, 0xffffffff, 'x', "SAI Transmit Configuration 2 Register"},
{ "I2S1_TCR3", 		0x0202800C, 0, 0xffffffff, 'x', "SAI Transmit Configuration 3 Register"},
{ "I2S1_TCR4", 		0x02028010, 0, 0xffffffff, 'x', "SAI Transmit Configuration 4 Register"},
{ "I2S1_TCR5", 		0x02028014, 0, 0xffffffff, 'x', "SAI Transmit Configuration 5 Register"},
{ "I2S1_TDR0", 		0x02028020, 0, 0xffffffff, 'x', "SAI Transmit Data Register"},
{ "I2S1_TFR0", 		0x02028040, 0, 0xffffffff, 'x', "SAI Transmit FIFO Register"},
{ "I2S1_TMR", 		0x02028060, 0, 0xffffffff, 'x', "SAI Transmit Mask Register"},
{ "I2S1_RCSR", 		0x02028080, 0, 0xffffffff, 'x', "SAI Receive Control Register"},
{ "I2S1_RCR1", 		0x02028084, 0, 0xffffffff, 'x', "SAI Receive Configuration 1 Register"},
{ "I2S1_RCR2", 		0x02028088, 0, 0xffffffff, 'x', "SAI Receive Configuration 2 Register"},
{ "I2S1_RCR3", 		0x0202808C, 0, 0xffffffff, 'x', "SAI Receive Configuration 3 Register "},
{ "I2S1_RCR4", 		0x02028090, 0, 0xffffffff, 'x', "SAI Receive Configuration 4 Register"},
{ "I2S1_RCR5", 		0x02028094, 0, 0xffffffff, 'x', "SAI Receive Configuration 5 Register"},
{ "I2S1_RDR0", 		0x020280A0, 0, 0xffffffff, 'x', "SAI Receive Data Register"},
{ "I2S1_RFR0", 		0x020280C0, 0, 0xffffffff, 'x', "SAI Receive FIFO Register"},
{ "I2S1_RMR", 		0x020280E0, 0, 0xffffffff, 'x', "SAI Receive Mask Register"},
{ "I2S2_TCSR", 		0x0202C000, 0, 0xffffffff, 'x', "SAI Transmit Control Register"},
{ "I2S2_TCR1", 		0x0202C004, 0, 0xffffffff, 'x', "SAI Transmit Configuration 1 Register"},
{ "I2S2_TCR2", 		0x0202C008, 0, 0xffffffff, 'x', "SAI Transmit Configuration 2 Register"},
{ "I2S2_TCR3", 		0x0202C00C, 0, 0xffffffff, 'x', "SAI Transmit Configuration 3 Register"},
{ "I2S2_TCR4", 		0x0202C010, 0, 0xffffffff, 'x', "SAI Transmit Configuration 4 Register"},
{ "I2S2_TCR5", 		0x0202C014, 0, 0xffffffff, 'x', "SAI Transmit Configuration 5 Register"},
{ "I2S2_TDR0", 		0x0202C020, 0, 0xffffffff, 'x', "SAI Transmit Data Register"},
{ "I2S2_TFR0", 		0x0202C040, 0, 0xffffffff, 'x', "SAI Transmit FIFO Register"},
{ "I2S2_TMR", 		0x0202C060, 0, 0xffffffff, 'x', "SAI Transmit Mask Register"},
{ "I2S2_RCSR", 		0x0202C080, 0, 0xffffffff, 'x', "SAI Receive Control Register"},
{ "I2S2_RCR1", 		0x0202C084, 0, 0xffffffff, 'x', "SAI Receive Configuration 1 Register"},
{ "I2S2_RCR2", 		0x0202C088, 0, 0xffffffff, 'x', "SAI Receive Configuration 2 Register"},
{ "I2S2_RCR3", 		0x0202C08C, 0, 0xffffffff, 'x', "SAI Receive Configuration 3 Register"},
{ "I2S2_RCR4", 		0x0202C090, 0, 0xffffffff, 'x', "SAI Receive Configuration 4 Register"},
{ "I2S2_RCR5", 		0x0202C094, 0, 0xffffffff, 'x', "SAI Receive Configuration 5 Register"},
{ "I2S2_RDR0", 		0x0202C0A0, 0, 0xffffffff, 'x', "SAI Receive Data Register"},
{ "I2S2_RFR0", 		0x0202C0C0, 0, 0xffffffff, 'x', "SAI Receive FIFO Register"},
{ "I2S2_RMR", 		0x0202C0E0, 0, 0xffffffff, 'x', "SAI Receive Mask Register"},
{ "I2S3_TCSR", 		0x02030000, 0, 0xffffffff, 'x', "SAI Transmit Control Register"},
{ "I2S3_TCR1", 		0x02030004, 0, 0xffffffff, 'x', "SAI Transmit Configuration 1 Register"},
{ "I2S3_TCR2", 		0x02030008, 0, 0xffffffff, 'x', "SAI Transmit Configuration 2 Register"},
{ "I2S3_TCR3", 		0x0203000C, 0, 0xffffffff, 'x', "SAI Transmit Configuration 3 Register"},
{ "I2S3_TCR4", 		0x02030010, 0, 0xffffffff, 'x', "SAI Transmit Configuration 4 Register"},
{ "I2S3_TCR5", 		0x02030014, 0, 0xffffffff, 'x', "SAI Transmit Configuration 5 Register"},
{ "I2S3_TDR0", 		0x02030020, 0, 0xffffffff, 'x', "SAI Transmit Data Register"},
{ "I2S3_TFR0", 		0x02030040, 0, 0xffffffff, 'x', "SAI Transmit FIFO Register"},
{ "I2S3_TMR", 		0x02030060, 0, 0xffffffff, 'x', "SAI Transmit Mask Register"},
{ "I2S3_RCSR", 		0x02030080, 0, 0xffffffff, 'x', "SAI Receive Control Register"},
{ "I2S3_RCR1", 		0x02030084, 0, 0xffffffff, 'x', "SAI Receive Configuration 1 Register"},
{ "I2S3_RCR2", 		0x02030088, 0, 0xffffffff, 'x', "SAI Receive Configuration 2 Register"},
{ "I2S3_RCR3", 		0x0203008C, 0, 0xffffffff, 'x', "SAI Receive Configuration 3 Register"},
{ "I2S3_RCR4", 		0x02030090, 0, 0xffffffff, 'x', "SAI Receive Configuration 4 Register"},
{ "I2S3_RCR5", 		0x02030094, 0, 0xffffffff, 'x', "SAI Receive Configuration 5 Register"},
{ "I2S3_RDR0", 		0x020300A0, 0, 0xffffffff, 'x', "SAI Receive Data Register"},
{ "I2S3_RFR0", 		0x020300C0, 0, 0xffffffff, 'x', "SAI Receive FIFO Register"},
{ "I2S3_RMR", 		0x020300E0, 0, 0xffffffff, 'x', "SAI Receive Mask Register"}
};
