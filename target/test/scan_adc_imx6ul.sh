#!/bin/sh
 
for channel in 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15; do
	value=`cat /sys/bus/iio/devices/iio\:device0/in_voltage${channel}_raw`
	value=$((value*3300/4096))
	echo "ADC1_IN${channel} =" ${value} "mV"
done
