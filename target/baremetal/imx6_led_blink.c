/*
	Inspired from imx6q_uart.c (http://atose.org/?p=141) which is:
	Copyright (c) 2012-2013 Andrew Trotman
	Licensed BSD
*/

#include <stdint.h>

#include "imx6_uart.h"
#include "imx6_epit.h"
#include "apf6dev_led.h"

void enable_pins(void)
{
	/* If not launched from U-Boot, here should be defined the IOMUX
	configuration for used GPIOs and UART */
}

void enable_clocks(void)
{
	/* If not launched from U-Boot, here should be defined the clock
	configuration for UART and EPIT */
}

#define DELAY_US 500000	/* 0.5 secs */

int main(void)
{
	int i = 20;

	enable_clocks();
	enable_pins();
	serial_init();
	epit_init();
	apf6dev_userled_init();

	epit_delay_us(DELAY_US);
	debug_puts("\r\nExample led_blink compiled on " __DATE__ " at " __TIME__ "\r\n");

	/* Infinite loop */
	while (i != 0) {
		apf6dev_userled_set(1);
		epit_delay_us(DELAY_US);
		apf6dev_userled_set(0);
		epit_delay_us(DELAY_US);
	}

	return 0;
}
