/*
        Inspired from imx6q_uart.c (http://atose.org/?p=141), which is:
	Copyright (c) 2012-2013 Andrew Trotman
	Licensed BSD
*/

#include <stdint.h>

#define BAUD_RATE 115200
#define DEFAULT_UART 4		/* Console on APF6Dev */

static volatile unsigned long *uart4_utxd = (unsigned long *)0x021f0040;
static volatile unsigned long *uart4_usr2 = (unsigned long *)0x021f0098;
#define B_USR2_TXFE	(1 << 14)

void debug_putc(char value)
{
	/* Write to the serial port */
	*uart4_utxd = value;

	/* Make sure it was sent */
	while ((*uart4_usr2 & B_USR2_TXFE) == 0)
		; /* do nothing */
}

void debug_puts(char *string)
{
	while (*string != 0)
		debug_putc(*string++);
}

#if 0
char debug_getc(void)
{
/*
	Wait for a character to arrive
*/
	while ((HW_UART_USR2(DEFAULT_UART).B.RDR) == 0)
	; // do nothing

/*
	Get it and put it into the array
*/
	return HW_UART_URXD_RD(DEFAULT_UART) & 0xFF;
}
#endif

#define PLL3_FREQUENCY 80000000

void serial_init(void)
{
#if 0
TBDL !!!
/*
	Disable and soft reset the UART then wait for it to come up
*/
HW_UART_UCR1(DEFAULT_UART).U = 0;						// disable the UART
HW_UART_UCR2(DEFAULT_UART).U = 0;						// software reset (SRST)
while (HW_UART_UCR2(DEFAULT_UART).B.SRST == 0)
	;	// nothing

/*
	Enable the UART
	Enable RXDMUXSEL (must be on)
*/
HW_UART_UCR1(DEFAULT_UART).B.UARTEN = 1;
HW_UART_UCR3(DEFAULT_UART).B.RXDMUXSEL = 1;

/*
	8 bits, 1 stop bit, no parity,software flow control
*/
/*                               8-bits             ignore RTS         enable RX            enable TX         don't reset */
HW_UART_UCR2_WR(DEFAULT_UART, BM_UART_UCR2_WS | BM_UART_UCR2_IRTS | BM_UART_UCR2_RXEN | BM_UART_UCR2_TXEN | BM_UART_UCR2_SRST);

/*
	Set the board rate

	The "Module Clock" is the UART_CLK which comes from CCM.
	The "Peripheral Clock" is the IPG_CLK which comes from CCM.

	PLL3 runs at 80MHz by default
	PLL3 -> CDCDR1:uart_clk_podf (6 bit divider) -> UART_CLK_ROOT
*/

/*
	Divide the clock by 2
*/
HW_UART_UFCR(DEFAULT_UART).B.RFDIV = 0x04;		/* divide input clock by 2 */
HW_UART_UFCR(DEFAULT_UART).B.DCEDTE = 0;		/* DCE mode */


/*
	Binary Rate Multiplier Numerator = 0x0F
*/
HW_UART_UBIR(DEFAULT_UART).U = 0x0F;

/*
	Binary Rate Multipier Denominator set based on the baud rate
*/
HW_UART_UBMR(DEFAULT_UART).U = (PLL3_FREQUENCY / (HW_CCM_CSCDR1.B.UART_CLK_PODF + 1)) / (2 * BAUD_RATE);		// UBMR should be 0x015B once set
#endif
}

