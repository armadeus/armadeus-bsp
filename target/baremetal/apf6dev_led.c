#include <stdint.h>

#ifdef DEBUG
#include "imx6_uart.h"
#endif

#define GPIO7_DR	(*(volatile uint32_t *)0x020b4000)
#define GPIO7_GDIR	(*(volatile uint32_t *)0x020b4004)
#define GPIO_PIN12	(1 << 12)

void apf6dev_userled_init(void)
{
	uint32_t reg;

#ifdef DEBUG
	debug_puts("led init\r\n");
#endif
	reg = GPIO7_GDIR;
	reg |= GPIO_PIN12;
	GPIO7_GDIR = reg;
}

void apf6dev_userled_set(int state)
{
	uint32_t reg;

	if (state) {
		reg = GPIO7_DR;
		reg |= GPIO_PIN12;
		GPIO7_DR = reg;
	} else {
		reg = GPIO7_DR;
		reg &= ~GPIO_PIN12;
		GPIO7_DR = reg;
	}
}
