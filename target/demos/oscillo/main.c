/*
 * (Kind of) Oscilloscope for the APF  
 *
 * Copyright (C) 2009 <julien.boibessot@armadeus.com>
 *                    Armadeus Project / Armadeus Systems
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <math.h> /* sin() */

#include "lcd.h"
#include "as_devices/as_adc_iio.h"

int fbfd;

static void cleanup(void)
{
	close_lcd(fbfd);
}

#define MAX_CHANNELS 10
struct color black = {0, 0, 0};
struct color white = {0xff, 0xff, 0xff};

int main(int argc, char **argv)
{
	/*int x[MAX_CHANNELS] = {0, 10, 20, 30, 40, 50, 60, 70};*/
	int y[MAX_CHANNELS];
	int time = 0, i;
	int nb_channels, start_channel, xres, yres;
	double rad;
	struct color colours[MAX_CHANNELS] = {{120,120,120}, {0,150,150},
			{160,0,160}, {170,170,0}, {0,0,255}, {0,255,0},
			{255,0,0}, {255,255,255}, {255,255,0}, {255,0,255}};
	struct as_adc_device *adc;

	atexit(cleanup);

	fbfd = init_lcd();
	if (fbfd < 0)
		return EXIT_FAILURE;

	if (argc > 2) {
		start_channel = atoi(argv[1]);
		nb_channels = atoi(argv[2]);
	} else {
		start_channel = 0;
		nb_channels = 3;
	}
	if ((start_channel + nb_channels) > MAX_CHANNELS) {
		printf("Bad channel parameters !\n");
		exit(1);
	}

	xres = lcd_get_xres();
	yres = lcd_get_yres();

	adc = as_adc_open_iio(0, 0);
	if (adc)
		printf("Using real ADC (through AsDevices)\n");
	else
		printf("Using fake ADC\n");

	printf("Showing channel(s) %d to %d\n", start_channel, (start_channel + nb_channels - 1));
	while (1) {
		for (i=start_channel; i< (start_channel+nb_channels); i++) {
			if (adc) {
				y[i] = yres - 1 - ((as_adc_get_value_in_millivolts_iio(adc, i) * yres) / 4096);
			} else {
				rad += 0.01745f;
				y[i] = (unsigned int)(sin(rad)*100 + (yres/2) + (yres/10)*i);
				//printf("%d: %d %d\n", i, time, y[i]);
			}
			draw_pixel(time, y[i], colours[i]);
		}
		time += 1;
		if (time >= xres) {
			time = 0;
		}
		clear_vline(time, black);
		if (time < xres - 1)
			clear_vline(time+1, white);
		usleep(10000);
	}
	
	return EXIT_SUCCESS;
}
