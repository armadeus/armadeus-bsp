#!/usr/bin/env python

import serial
from time import sleep

temp_file = open("/sys/class/thermal/thermal_zone0/temp")
led_file = open("/sys/class/leds/User/brightness",'w')
led_trigger_file = open("/sys/class/leds/User/trigger",'w')
rfcomm = serial.Serial('/dev/rfcomm0', 9600, timeout=1)
tty_file = open("/dev/tty1", 'w')
stat_file =  open("/proc/stat")

led_state = 0
prev_total = 0
prev_idle = 0

def cpu_usage():

	global prev_total
	global prev_idle

	line = stat_file.readline()
	stat_file.seek(0)
	list = line.split()

	idle = int(list[3])
	del list[0]
	total = 0
	for value in list:
		total = total + int(value)

	diff_idle = idle - prev_idle
	diff_total = total - prev_total
	diff_usage = (1000*(diff_total - diff_idle) / diff_total + 5)/10

	print("CPU: %d " % diff_usage)

	prev_total = total
	prev_idle = idle

	return (100-diff_usage)

led_trigger_file.write("gpio")
led_trigger_file.flush()

while True:
#	sleep(1)
	# Temperature handling stuff:
	temp = temp_file.read(5)
	temp_file.seek(0)
	temp = int(temp)
	temp = int(temp / 1000)
	print("temp: %d C" % temp)
	temp_str = b'*T%d*' % temp
	rfcomm.write(temp_str)

	# Android's cmmands handling:
	# "g" -> button release
	# "Sxxxxxx*" -> text string receive
	data = rfcomm.read(1)
#	print(data)
	if data == b'g':
		led_state = not led_state
		led = "%d" % led_state
		print(led)
		led_file.write(led)
		led_file.flush()
	if data == b'S':
		text = ""
		while True:
			data = rfcomm.read(1)
			if data == b'*':
				break
			text = text + data.decode('utf-8')
		text = text + "\n"
#		print(text)
		tty_file.write(text)
		tty_file.flush()

	# CPU usage stuff
	cpu = cpu_usage()
	cpu_text = b'*G%d*' % cpu
	rfcomm.write(cpu_text)
