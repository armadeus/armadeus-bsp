/*
 *  Armadeus V4L2/SDL video capture example
 *
 * Inspired a lot from:
 * - http://www.linuxtv.org/downloads/video4linux/API/V4L2_API/v4l2spec/capture.c
 * - http://freshmeat.net/projects/libv4l
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <getopt.h>             /* getopt_long() */
#include <fcntl.h>              /* low-level i/o */
#include <unistd.h>
#include <errno.h>
#include <malloc.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <sys/wait.h>		/* signal */
#include <asm/types.h>          /* for videodev2.h */

#include <linux/videodev2.h>
#include <SDL/SDL.h>		/* TODO: make it compatible with Framebuffer only */

#ifdef USE_NEON
#include <bayer2rgb/bayer2rgb.h>
#endif

#ifdef USE_VPU
#include "vpu_codec.h"
#endif

#include <turbojpeg.h>
#include "csc.h"

#define CLEAR(x) memset (&(x), 0, sizeof (x))

#define SCREEN_WIDTH 480
#define SCREEN_HEIGHT 272

#define CAM_WIDTH 320
#define CAM_HEIGHT 240

typedef enum {
	IO_METHOD_READ,
	IO_METHOD_MMAP,
	IO_METHOD_USERPTR,
} io_method;

struct buffer {
        void   *start;
        size_t length;
	size_t offset;
};

static char *dev_name = NULL;
static char *ip_addr = "192.168.0.2";
static io_method io = IO_METHOD_MMAP;
static int v4l_fd = -1;
struct buffer *buffers = NULL;
static unsigned int n_buffers = 0;
int g_use_codec = 0;
static int debug = 0;

SDL_Surface *screen = NULL, *image = NULL;

struct camera {
	unsigned int width;
	unsigned int height;
	unsigned int pixelformat;
	unsigned int bytesperline;
	unsigned int sizeimage;
};

struct camera mycamera;

unsigned int force_yuv420 = 0;
unsigned int force_fps = 0;
unsigned int force_crop = 0;
unsigned int video_input = 0;
unsigned int capture_mode = 0;
unsigned int crop_width = 0;
unsigned int crop_height = 0;

#define FOURCC_TO_STR(fmt) fmt & 0xff, (fmt >> 8) & 0xff, (fmt >> 16) & 0xff, (fmt >> 24) & 0xff

static void start_capturing(void);
static void stop_capturing(void);

static void errno_exit(const char *s)
{
	fprintf(stderr, "%s error %d, %s\n", s, errno, strerror (errno));
	exit(EXIT_FAILURE);
}

static int xioctl(int fd, int request, void *arg)
{
        int r;

        do r = ioctl (fd, request, arg);
        while (-1 == r && EINTR == errno);

        return r;
}

static Uint16 inline yuv2rgb(int Y, int U, int V)
{
	int R, G, B;

	R = Y + 1.4026 * (V-128);
	G = Y - 0.3444 * (U-128) - 0.7144 * (V-128);
	B = Y + 1.7730 * (U-128);

	if (R > 255) R = 255;
	if (G > 255) G = 255;
	if (B > 255) B = 255;
	if (R < 0) R = 0;
	if (G < 0) G = 0;
	if (B < 0) B = 0;

	return ((R>>3) << 11) | ((G>>2) << 5) | (B>>3);
}

#define STRIP(X) (X = X > 255 ? 255 : (X < 0) ? 0 : X)

static Uint8 inline YUV2R(int Y, int U, int V)
{
	int R;

	R = Y + 1.4026 * (V-128);
	STRIP(R);

	return R;
}

static Uint8 inline YUV2G(int Y, int U, int V)
{
	int G;

	G = Y - 0.3444 * (U-128) - 0.7144 * (V-128);
	STRIP(G);

	return G;
}

static Uint16 inline YUV2B(int Y, int U, int V)
{
	int B;

	B = Y + 1.7730 * (U-128);
	STRIP(B);

	return B;
}
#if 0
static void yuv422_to_greyscale(void *yuv, int size, void *grey)
{
	unsigned int Y, Y2, yuv32;
	int i;
	Uint8 *dest = (Uint8*) grey;
	Uint32 *src = (Uint32*) yuv;

	for (i=size/4; i; i--) {
		yuv32 = *(src++);

		/* takes Ys from YUV422 packed format */
		Y = (yuv32 & 0x000000ff);
		Y2 = (yuv32 & 0x00ff0000) >> 16;

		*(dest++) = (Uint8)Y;
		*(dest++) = (Uint8)Y2;
	}
}
#endif

static void yuyv_to_rgb565(void *yuv, int size, void *rgb)
{
	unsigned int Y, Y2, U, V, yuv32, rgb32;
	int R, G, B;
	int i;
	Uint32 *dest = (Uint32*) rgb;
	Uint32 *src = (Uint32*) yuv;

	for (i=size/4; i; i--)
	{
		yuv32 = *(src++);

		/* packed format */
		Y = (yuv32 & 0x000000ff);
		U = (yuv32 & 0x0000ff00) >> 8;
		Y2 = (yuv32 & 0x00ff0000) >> 16;
		V = (yuv32 & 0xff000000) >> 24;

		R = YUV2R(Y, U, V);
		G = YUV2G(Y, U, V);
		B = YUV2B(Y, U, V);
		rgb32 = ((R>>3) << 11) | ((G>>2) << 5) | (B>>3);
// 		rgb32 = yuv2rgb(Y, U, V);

		R = YUV2R(Y2, U, V);
		G = YUV2G(Y2, U, V);
		B = YUV2B(Y2, U, V);
		rgb32 |= (((R>>3) << 11) | ((G>>2) << 5) | (B>>3)) << 16;
// 		rgb32 |= yuv2rgb(Y2, U, V) << 16;

		*(dest++) = rgb32;
	}
}

static void yuyv_to_rgb32(void *yuv, int size, void *rgb)
{
	unsigned int Y, Y2, U, V, yuv32, rgb32;
	int R, G, B;
	int i;
	Uint32 *dest = (Uint32*) rgb;
	Uint32 *src = (Uint32*) yuv;

	for (i=size/4; i; i--)
	{
		yuv32 = *(src++);

		/* packed format */
		Y = (yuv32 & 0x000000ff);
		U = (yuv32 & 0x0000ff00) >> 8;
		Y2 = (yuv32 & 0x00ff0000) >> 16;
		V = (yuv32 & 0xff000000) >> 24;

		R = YUV2R(Y, U, V);
		G = YUV2G(Y, U, V);
		B = YUV2B(Y, U, V);
		rgb32 = ((R << 16) | (G << 8) | B);
		*(dest++) = rgb32;

		R = YUV2R(Y2, U, V);
		G = YUV2G(Y2, U, V);
		B = YUV2B(Y2, U, V);
		rgb32 = ((R << 16) | (G << 8) | B);
		*(dest++) = rgb32;
	}
}

static void yuyv_to_rgb(void *yuv, int size, void *rgb, int bpp)
{
	if (bpp == 32) {
		yuyv_to_rgb32(yuv, size, rgb);
	} else if (bpp = 16) {
		yuyv_to_rgb565(yuv, size, rgb);
	} else {
		fprintf(stdout, "yuyv_to_rgb: unsupported screen bpp\n");
	}
}

static void uyvy_to_rgb32(void *yuv, int size, void *rgb)
{
	unsigned int Y, Y2, U, V, yuv32, rgb32;
	int R, G, B;
	int i;
	Uint32 *dest = (Uint32*) rgb;
	Uint32 *src = (Uint32*) yuv;

	for (i=size/4; i; i--)
	{
		yuv32 = *(src++);

		/* packed format */
		U = (yuv32 & 0x000000ff);
		Y = (yuv32 & 0x0000ff00) >> 8;
		V = (yuv32 & 0x00ff0000) >> 16;
		Y2 = (yuv32 & 0xff000000) >> 24;

		R = YUV2R(Y, U, V);
		G = YUV2G(Y, U, V);
		B = YUV2B(Y, U, V);
		//rgb32 = ((R>>3) << 11) | ((G>>2) << 5) | (B>>3);
		rgb32 = ((R << 16) | (G << 8) | B);
		*(dest++) = rgb32;
// 		rgb32 = yuv2rgb(Y, U, V);

		R = YUV2R(Y2, U, V);
		G = YUV2G(Y2, U, V);
		B = YUV2B(Y2, U, V);
		//rgb32 |= (((R>>3) << 11) | ((G>>2) << 5) | (B>>3)) << 16;
		rgb32 = ((R << 16) | (G << 8) | B);
// 		rgb32 |= yuv2rgb(Y2, U, V) << 16;

		*(dest++) = rgb32;
	}
}

static void uyvy_to_rgb(void *yuv, int size, void *rgb, int bpp)
{
	if (bpp == 32) {
		uyvy_to_rgb32(yuv, size, rgb);
	} else if (bpp = 16) {
		yuyv_to_rgb565(yuv, size, rgb);
	} else {
		fprintf(stdout, "uyvy_to_rgb: unsupported screen bpp\n");
	}
}

/* Only grayscale conversion for the moment
   As yuv420 is slightly complicated to decode it would be worth using libv4l
*/
static void yuv420_to_rgb565(void *yuv, int size, void *rgb)
{
	Uint8 yuv8;
	int R, G, B;
	int i;
	int nbpix = mycamera.width * mycamera.height;
	Uint16 *dest = (Uint16*) rgb;
	Uint8 *src = (Uint8*) yuv;

	nbpix = mycamera.width * mycamera.height;
	for (i=nbpix; i; i--) {
		yuv8 = *(src++);
		R = G = B = yuv8;
		*(dest++) = ((R>>3) << 11) | ((G>>2) << 5) | (B>>3);;
	}
}

static void yuv420_to_rgb32(void *yuv, int size, void *rgb)
{
	Uint8 yuv8;
	Uint32 yuv32;
	Uint8 R, G, B;
	int i, j;
	int nbpix = mycamera.width * mycamera.height;
	Uint16 *dest16 = (Uint16*) rgb;
	Uint32 *dest32 = (Uint32*) rgb;
	Uint8 *src = (Uint8*) yuv;

	nbpix = mycamera.width * mycamera.height;
	for (i=nbpix; i; i--) {
		yuv8 = *(src++);
		R = G = B = yuv8;
		*(dest32++) = (R << 16) | (G << 8) | (B); /* BGRA */
	}
}

static void yuv420_to_rgb(void *yuv, int size, void *rgb, int bpp)
{
	if (bpp == 32) {
		yuv420_to_rgb32(yuv, size, rgb);
	} else if (bpp = 16) {
		yuv420_to_rgb565(yuv, size, rgb);
	} else {
		fprintf(stdout, "yuv420_to_rgb: unsupported screen bpp\n");
	}
}

static void rgb24_to_rgb(void *rgb_src, int size, void *rgb_dst, int bpp)
{
	Uint32 *dest32 = (Uint32*)rgb_dst;
	Uint8 *src = (Uint8 *)rgb_src;
	int i, pos = 0;
	Uint8 R, G, B, A;
	Uint32 color;

	if (bpp == 32) {
		for (i = size/4; i; i--) {
			R = src[pos++];
			G = src[pos++];
			B = src[pos++];
			*dest32++ = (R << 16) | (G << 8) | B;
		}
	} else {
		fprintf(stdout, "%s: unsupported screen bpp (%d)\n", __func__, bpp);
	}
}

static void rgb565_to_rgb(void *rgb_src, int size, void *rgb_dst, int bpp)
{
	Uint32 *dest32 = (Uint32*)rgb_dst;
	Uint16 *src = (Uint16 *)rgb_src;
	int i, pos = 0;
	Uint8 R, G, B, A;
	Uint16 pixel;
	static int first_time = 1;

	if (first_time) {
		fprintf(stdout, "%s: image size %d\n", __func__, size);
		first_time = 0;
	}

	if (bpp == 32) {
		for (i = size/2; i; i--) {
			pixel = src[pos];
			B = pixel >> 8;
			G = (pixel & 0x07e0) >> 3;
			R = (pixel & 0x1f) << 3;
			*dest32++ = (R << 16) | (G << 8) | B;
			pos++;
		}
	} else {
		fprintf(stdout, "%s: unsupported screen bpp (%d)\n", __func__, bpp);
	}
}

#if 0
static void rgb565_to_yuv420(void *rgb, int size, void *yuv)
{
	int Y, U, V;
	int R, G, B;
	int i, index = 1;
	int nbpix = mycamera.width * mycamera.height;
	Uint16 *src = (Uint16*) rgb;
	Uint8 *dest = (Uint8*) yuv;

	for (i = 0; i < nbpix; i++) {
		R = (src[i] & 0xf800) >> 8;
		G = (src[i] & 0x07c0) >> 3;
		B = (src[i] & 0x001f) << 3;
		Y = 0.299 * R + 0.587 * G + 0.114 * B;
		dest[i] = Y;
		if ((index == 1) || (index == 3) || (index == 5)) {
			U = -0.169 * R - 0.331 * G + 0.449 * B + 128;
			V = 0.499 * R - 0.418 * G - 0.0813 * B + 128;
			dest[i + nbpix / 2] = U;
			dest[i + 3 * nbpix / 4] = V;
		}
		index++;
		if (index >= 12)
			index = 1;
	}
}
#endif

static void rgb565_to_yuv420(void *rgb, int size, void *yuv)
{
	int Y, U, V;
	int R, G, B;
	int i, index = 0;
	int x, y, u = 0, v = 0;
	int nbpix = mycamera.width * mycamera.height;
	Uint16 *src = (Uint16*) rgb;
	Uint8 *dest = (Uint8*) yuv;

	for (i = 0; i < nbpix; i++) {
		R = (src[i] & 0xf800) >> 8;
		G = (src[i] & 0x07e0) >> 3;
		B = (src[i] & 0x001f) << 3;
		Y = 0.299 * R + 0.587 * G + 0.114 * B;
		dest[i] = Y;
	}
	for (y = 0; y < mycamera.height ; y++) {
		if ((y & 0x00000001) == 0) {
			for (x = 0; x < mycamera.width ; x = x + 2) {
				R = (src[x] & 0xf800) >> 8;
				G = (src[x] & 0x07e0) >> 3;
				B = (src[x] & 0x001f) << 3;
				U = -0.169 * R - 0.331 * G + 0.449 * B + 128;
				V = 0.499 * R - 0.418 * G - 0.0813 * B + 128;
				dest[nbpix + u] = U;
				u++;
				dest[nbpix + nbpix / 4 + v] = V;
				v++;
			}
		}
	}
}

static void jpeg_to_rgb(int desired_width, int desired_height,
		unsigned char *destination, unsigned char *jpg_data,
		unsigned int jpg_size)
{
    tjhandle decompressor = tjInitDecompress();
    int subsamp, width, height, error;

    error = tjDecompressHeader2(decompressor,
        jpg_data,
        jpg_size,
        &width,
        &height,
        &subsamp);

    if (error) {
        printf("Failed to decode JPEG: %s", tjGetErrorStr());
        tjDestroy(decompressor);
        return;
    }

    error = tjDecompress2(decompressor,
        jpg_data,
        jpg_size,
        destination,
        desired_width,
        0,
        desired_height,
        TJPF_BGRX,
        TJFLAG_FASTDCT);

    if (error) {
        printf("Failed to decode JPEG: %s", tjGetErrorStr());
        tjDestroy(decompressor);
        return;
    }

    tjDestroy(decompressor);
    return;
}

static void simple_bayer_to_rgb(void *bayer_src, int size, int line_size, int img_pix_height, void *rgb_dst, int dst_bpp)
{
	Uint32 *dest32 = (Uint32*)rgb_dst;
	Uint8 *src = (Uint8 *)bayer_src;
	int i, j, pos = 0;
	Uint8 R, G, B, A;
	Uint16 pixel;
	static int first_time = 1;

	if (first_time) {
		fprintf(stdout, "%s: image size %d line size %d\n", __func__, size, line_size);
		first_time = 0;
	}

	if (dst_bpp == 32) {
		for (j = 0; j < img_pix_height-1; j++) {
			for (i = 0; i < line_size; i++) {
				B = src[i + (j*line_size)];
				G = src[i+1 + (j*line_size)];
				R = src[i+1 + line_size + (j*line_size)];
				*dest32++ = (R << 16) | (G << 8) | B;
				i++;
				B = src[i-1 + (j*line_size)];
				G = src[i + (j*line_size)];
				R = src[i + line_size + (j*line_size)];
				*dest32++ = (R << 16) | (G << 8) | B;
			}
		}
	} else {
		fprintf(stdout, "%s: unsupported screen bpp (%d)\n", __func__, dst_bpp);
	}
	//fprintf(stdout, "%s: %d x %d\n", __func__, i, j);
}

#ifdef USE_NEON
struct x_image_in {
	struct image_in		image;
	void const		*memory;
	size_t			total_size;
};

struct x_image_out {
	struct image_out	image;
	void const		*memory;
	size_t			total_size;
};

static void neon_bayer_to_rgb(void *bayer_src, int size, int line_size, int img_pix_height, void *rgb_dst, int dst_bpp)
{
	struct image_in src;
	struct image_out dst;

	src = (struct image_in) {
			.data = (uint8_t *)bayer_src,
			.info	= {
				.bpp		= 8,
				.endian		= BAYER_E_LITTLE,
				.w		= 640,
				.h		= 480,
				.stride		= line_size,
			},
	};
	src.type = BAYER_BGGR;

	dst = (struct image_out) {
		.data = rgb_dst,
		.quality = 0,
		.info	= {
			.bpp		= 32,
			.w		= 640,
			.h		= 480,
			.stride		= 640*4,
		},
	};
	dst.quality = ~0ul;

	bayer2rgb_convert_neon(&src, &dst, NULL);
}
#endif

static void simple_copy(void *yuv, int size, void* rgb)
{
	memcpy(rgb, yuv, size);
}

static void process_image(/*const */void *pCaptured, int size)
{
	SDL_Rect update_rec;
	int sec, usec, run_time;
	static struct timeval now, last;
	static int fps;
	static int first_time = 1;

	if (mycamera.pixelformat == V4L2_PIX_FMT_YUYV) {
		yuyv_to_rgb(pCaptured, mycamera.sizeimage, image->pixels, screen->format->BitsPerPixel);
	} else if (mycamera.pixelformat == V4L2_PIX_FMT_UYVY) {
		uyvy_to_rgb(pCaptured, mycamera.sizeimage, image->pixels, screen->format->BitsPerPixel);
	} else if (mycamera.pixelformat == V4L2_PIX_FMT_YUV420) {
		yuv420_to_rgb(pCaptured, mycamera.sizeimage, image->pixels, screen->format->BitsPerPixel);
	} else if (mycamera.pixelformat == V4L2_PIX_FMT_RGB24) {
		rgb24_to_rgb(pCaptured, mycamera.sizeimage, image->pixels, screen->format->BitsPerPixel);
	} else if (mycamera.pixelformat == V4L2_PIX_FMT_RGB565) {
		rgb565_to_rgb(pCaptured, mycamera.sizeimage, image->pixels, screen->format->BitsPerPixel);
        } else if (mycamera.pixelformat == V4L2_PIX_FMT_JPEG) {
		jpeg_to_rgb(mycamera.width, mycamera.height, image->pixels, (char *)pCaptured, size);
        } else if (mycamera.pixelformat == V4L2_PIX_FMT_SRGGB8) {
#ifdef USE_NEON
		neon_bayer_to_rgb(pCaptured, mycamera.sizeimage, mycamera.width, mycamera.height, image->pixels, screen->format->BitsPerPixel);
#else
		simple_bayer_to_rgb(pCaptured, mycamera.sizeimage, mycamera.width, mycamera.height, image->pixels, screen->format->BitsPerPixel);
#endif
        } else if (mycamera.pixelformat == V4L2_PIX_FMT_SGRBG8) {
		BAYERToRGB(&_GR8, &_BRG32,
                               (char *)pCaptured, image->pixels, mycamera.width, mycamera.height,
                               0.5, 0.5, 0.5, 1);
	} else {
		if (mycamera.pixelformat != V4L2_PIX_FMT_RGB32) {
			if (first_time) {
				printf("Unknown camera image format (%c%c%c%c)!!\n", FOURCC_TO_STR(mycamera.pixelformat));
				first_time = 0;
			}
		}
		if (size == 0)
			size = mycamera.sizeimage;
		simple_copy(pCaptured, size, image->pixels);
	}

	/* Blit converted image on screen */
	update_rec.x = 0;
	update_rec.y = 0;
	update_rec.w = mycamera.width;
	update_rec.h = mycamera.height;
	SDL_BlitSurface(image, NULL, screen, &update_rec);
	SDL_Flip(screen);

	/* Calculate FPS */
	gettimeofday(&now, NULL);
	if (now.tv_sec == last.tv_sec) {
		fps++;
	} else {
		printf("FPS = %d\r", fps);
		fflush(stdout);
		fps = 0;
	}
	sec = now.tv_sec - last.tv_sec;
	usec = now.tv_usec - last.tv_usec;
	if (usec < 0) {
		sec--;
		usec = usec + 1000000;
	}
	run_time = (sec * 1000000) + usec;
//	printf("Run time %d ms (%d, %d) (%d, %d)\n", run_time, now.tv_sec, now.tv_usec, last.tv_sec, last.tv_usec);
	last.tv_sec = now.tv_sec;
	last.tv_usec = now.tv_usec;
}

static int read_frame(void)
{
        struct v4l2_buffer buf;
	unsigned int i;

	switch (io) {
	case IO_METHOD_READ:
		if (-1 == read(v4l_fd, buffers[0].start, buffers[0].length)) {
            		switch (errno) {
            		case EAGAIN:
                    		return 0;

			case EIO:
				/* Could ignore EIO, see spec. */

				/* fall through */

			default:
				errno_exit ("read");
			}
		}

    		process_image(buffers[0].start, buffers[0].length);

		break;

	case IO_METHOD_MMAP:
		CLEAR (buf);

            	buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
            	buf.memory = V4L2_MEMORY_MMAP;

		if (-1 == xioctl(v4l_fd, VIDIOC_DQBUF, &buf)) {
            		switch (errno) {
            		case EAGAIN:
                    		return 0;

			case EIO:
				/* Could ignore EIO, see spec. */

				/* fall through */

			default:
				errno_exit("VIDIOC_DQBUF");
			}
		}

                assert (buf.index < n_buffers);

#ifdef USE_VPU
		if (g_use_codec) {
			if (mycamera.pixelformat == V4L2_PIX_FMT_RGB565) {
				rgb565_to_yuv420(buffers[buf.index].start, mycamera.sizeimage, image->pixels);
			}
			vpu_codec_encode_next_frame(buffers[buf.index].offset, /*buffers[buf.index].start*/ image->pixels);
		}
#endif
// 		else
		process_image(buffers[buf.index].start, buf.length);

		if (-1 == xioctl(v4l_fd, VIDIOC_QBUF, &buf))
			errno_exit("VIDIOC_QBUF");

		break;

	case IO_METHOD_USERPTR:
		CLEAR (buf);

    		buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    		buf.memory = V4L2_MEMORY_USERPTR;

		if (-1 == xioctl(v4l_fd, VIDIOC_DQBUF, &buf)) {
			switch (errno) {
			case EAGAIN:
				return 0;

			case EIO:
				/* Could ignore EIO, see spec. */

				/* fall through */

			default:
				errno_exit ("VIDIOC_DQBUF");
			}
		}

		for (i = 0; i < n_buffers; ++i)
			if (buf.m.userptr == (unsigned long) buffers[i].start
			    && buf.length == buffers[i].length)
				break;

		assert (i < n_buffers);

		process_image((void *) buf.m.userptr, 0);

		if (-1 == xioctl(v4l_fd, VIDIOC_QBUF, &buf))
			errno_exit ("VIDIOC_QBUF");

		break;
	}

	return 1;
}

static int do_cropping(unsigned int xoff, unsigned int yoff, unsigned int factor)
{
	struct v4l2_cropcap cropcap;
	struct v4l2_crop crop;
	struct v4l2_rect cropping;

	CLEAR(cropcap);
	cropcap.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

	cropping.left = xoff;
	cropping.top = yoff;
	cropping.width = crop_width + factor;
	cropping.height = crop_height + factor;

	if (0 == xioctl(v4l_fd, VIDIOC_CROPCAP, &cropcap)) {
		crop.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		if (!force_crop)
			crop.c = cropcap.defrect; /* reset to default */
		else
			crop.c = cropping;

		if (-1 == xioctl(v4l_fd, VIDIOC_S_CROP, &crop)) {
			switch (errno) {
				case EINVAL:
				/* Cropping not supported. */
				break;
			default:
				/* Errors ignored. */
				break;
			}
		}
	} else {
		fprintf(stderr, "Camera (driver) doesn't support cropping\n");
		/* Errors ignored. */
	}

	return 0;
}

static int go_on = 1;
static int pause_capture = 0;

static void mainloop(void)
{
	SDL_Event event;
	struct stat exec_stat;
	int ret;
	static unsigned int xoff = 0;
	static unsigned int yoff = 0;
	static unsigned int factor = 0;

        while (go_on) {
		{
                        fd_set fds;
                        struct timeval tv;
                        int r;

                        FD_ZERO (&fds);
                        FD_SET (v4l_fd, &fds);

                        /* Timeout. */
                        tv.tv_sec = 2;
                        tv.tv_usec = 0;

                        r = select(v4l_fd + 1, &fds, NULL, NULL, &tv);

                        if (-1 == r) {
                                if (EINTR == errno)
                                        continue;

                                errno_exit ("select");
                        }

                        if (0 == r) {
                                fprintf (stderr, "select timeout\n");
                                exit (EXIT_FAILURE);
                        }

			read_frame();
                }

		while (SDL_PollEvent(&event)) {
			switch (event.type)
			{
				case SDL_MOUSEBUTTONUP:
				{
					ret = stat("/usr/local/bin/capture_hook", &exec_stat);
					if (ret == 0) {
						if (S_IXUSR & exec_stat.st_mode) {
							system("/usr/local/bin/capture_hook &");
						} else {
							fprintf(stderr, "can't execute capture hook\n");
						}
					}
					break;
				}

				case SDL_KEYDOWN:
					switch (event.key.keysym.sym) {
						case SDLK_SPACE:
							pause_capture = pause_capture ? 0 : 1;
							break;
						case SDLK_LEFT:
							xoff -= 10;
							break;
						case SDLK_RIGHT:
							xoff += 10;
							break;
						case SDLK_UP:
							yoff -= 10;
							break;
						case SDLK_DOWN:
							yoff += 10;
							break;
						case SDLK_PAGEUP:
							factor += 64;
							break;
						case SDLK_PAGEDOWN:
							factor -= 64;
							break;
						default:
							break;
					}
					if (xoff >= 500)
						xoff = 0;
					if (yoff >= 500)
						yoff = 0;
					if (factor >= 500)
						factor = 0;
					do_cropping(xoff, yoff, factor);

					break;

				case SDL_QUIT:
					go_on = 0;
					break;
			}
		}

		if (pause_capture)
			stop_capturing();
		else
			start_capturing();
        }
}

static int stream_on = 0;

static void stop_capturing(void)
{
        enum v4l2_buf_type type;

	if (!stream_on)
		goto stop_end;

	switch (io) {
	case IO_METHOD_READ:
		/* Nothing to do. */
		break;

	case IO_METHOD_MMAP:
	case IO_METHOD_USERPTR:
		type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

		if (-1 == xioctl(v4l_fd, VIDIOC_STREAMOFF, &type))
			errno_exit ("VIDIOC_STREAMOFF");

		break;
	}
	stream_on = 0;

stop_end:
	return;
}

static void start_capturing(void)
{
        unsigned int i;
        enum v4l2_buf_type type;

	if (stream_on)
		goto start_end;

	switch (io) {
	case IO_METHOD_READ:
		/* Nothing to do. */
		break;

	case IO_METHOD_MMAP:
		for (i = 0; i < n_buffers; ++i) {
            		struct v4l2_buffer buf;

        		CLEAR (buf);

        		buf.type        = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        		buf.memory      = V4L2_MEMORY_MMAP;
        		buf.index       = i;

			if (-1 == xioctl(v4l_fd, VIDIOC_QBUF, &buf))
                    		errno_exit ("VIDIOC_QBUF");
		}
		
		type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

		if (-1 == xioctl(v4l_fd, VIDIOC_STREAMON, &type))
			errno_exit ("VIDIOC_STREAMON");

		break;

	case IO_METHOD_USERPTR:
		for (i = 0; i < n_buffers; ++i) {
            		struct v4l2_buffer buf;

        		CLEAR (buf);

        		buf.type        = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        		buf.memory      = V4L2_MEMORY_USERPTR;
			buf.index       = i;
			buf.m.userptr	= (unsigned long) buffers[i].start;
			buf.length      = buffers[i].length;

			if (-1 == xioctl(v4l_fd, VIDIOC_QBUF, &buf))
                    		errno_exit ("VIDIOC_QBUF");
		}

		type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

		if (-1 == xioctl(v4l_fd, VIDIOC_STREAMON, &type))
			errno_exit ("VIDIOC_STREAMON");

		break;
	}

	stream_on = 1;

start_end:
	return;
}

static void uninit_device(void)
{
        unsigned int i;

	switch (io) {
	case IO_METHOD_READ:
		free (buffers[0].start);
		break;

	case IO_METHOD_MMAP:
		for (i = 0; i < n_buffers; ++i)
			if (-1 == munmap (buffers[i].start, buffers[i].length))
				errno_exit ("munmap");
		break;

	case IO_METHOD_USERPTR:
		for (i = 0; i < n_buffers; ++i)
			free (buffers[i].start);
		break;
	}

	free (buffers);
}

static void init_read(unsigned int buffer_size)
{
        buffers = calloc (1, sizeof (*buffers));

        if (!buffers) {
                fprintf (stderr, "Out of memory\n");
                exit (EXIT_FAILURE);
        }

	buffers[0].length = buffer_size;
	buffers[0].start = malloc (buffer_size);

	if (!buffers[0].start) {
    		fprintf (stderr, "Out of memory\n");
            	exit (EXIT_FAILURE);
	}
}

static void init_mmap(void)
{
	struct v4l2_requestbuffers req;

        CLEAR (req);

        req.count               = 4;
        req.type                = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        req.memory              = V4L2_MEMORY_MMAP;

	if (-1 == xioctl(v4l_fd, VIDIOC_REQBUFS, &req)) {
                if (EINVAL == errno) {
                        fprintf (stderr, "%s does not support "
                                 "memory mapping\n", dev_name);
                        exit (EXIT_FAILURE);
                } else {
                        errno_exit ("VIDIOC_REQBUFS");
                }
        }

        if (req.count < 2) {
                fprintf (stderr, "Insufficient buffer memory on %s\n",
                         dev_name);
                exit (EXIT_FAILURE);
        }

        buffers = calloc(req.count, sizeof (*buffers));

        if (!buffers) {
                fprintf (stderr, "Out of memory\n");
                exit (EXIT_FAILURE);
        }

        for (n_buffers = 0; n_buffers < req.count; ++n_buffers) {
                struct v4l2_buffer buf;

                CLEAR (buf);

                buf.type        = V4L2_BUF_TYPE_VIDEO_CAPTURE;
                buf.memory      = V4L2_MEMORY_MMAP;
                buf.index       = n_buffers;

                if (-1 == xioctl(v4l_fd, VIDIOC_QUERYBUF, &buf))
                        errno_exit ("VIDIOC_QUERYBUF");

                buffers[n_buffers].length = buf.length;
		buffers[n_buffers].offset = (size_t) buf.m.offset;
                buffers[n_buffers].start =
                        mmap (NULL /* start anywhere */,
                              buf.length,
                              PROT_READ | PROT_WRITE /* required */,
                              MAP_SHARED /* recommended */,
                              v4l_fd, buf.m.offset);
		/*printf("### start= 0x%08x offset=0x%08x\n", (unsigned int)(buffers[n_buffers].start), buffers[n_buffers].offset);*/
                if (MAP_FAILED == buffers[n_buffers].start)
                        errno_exit ("mmap");
        }
}

static void init_userp(unsigned int buffer_size)
{
	struct v4l2_requestbuffers req;
        unsigned int page_size;

        page_size = getpagesize ();
        buffer_size = (buffer_size + page_size - 1) & ~(page_size - 1);

        CLEAR (req);

        req.count               = 4;
        req.type                = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        req.memory              = V4L2_MEMORY_USERPTR;

        if (-1 == xioctl(v4l_fd, VIDIOC_REQBUFS, &req)) {
                if (EINVAL == errno) {
                        fprintf (stderr, "%s does not support "
                                 "user pointer i/o\n", dev_name);
                        exit(EXIT_FAILURE);
                } else {
                        errno_exit("VIDIOC_REQBUFS");
                }
        }

        buffers = calloc(4, sizeof (*buffers));

        if (!buffers) {
                fprintf (stderr, "Out of memory\n");
                exit (EXIT_FAILURE);
        }

        for (n_buffers = 0; n_buffers < 4; ++n_buffers) {
                buffers[n_buffers].length = buffer_size;
                buffers[n_buffers].start = memalign (/* boundary */ page_size,
                                                     buffer_size);

                if (!buffers[n_buffers].start) {
			fprintf(stderr, "Out of memory\n");
			exit(EXIT_FAILURE);
		}
        }
}

static Uint32 get_a_supported_pix_fmt(void)
{
	struct v4l2_fmtdesc fmtdesc;
	struct v4l2_frmsizeenum fszenum;
	struct v4l2_streamparm parm;
	Uint32 ret = 0;
	int i;

	/* First check if a LCD native format is available (-> no need for CSC) */
	for (i = 0;; i++) {
		CLEAR(fmtdesc);
		fmtdesc.index = i;
		fmtdesc.type  = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		if (-1 == ioctl(v4l_fd, VIDIOC_ENUM_FMT, &fmtdesc))
			break;
		if (debug) {
			fprintf(stdout, "Camera supports %c%c%c%c (%s) as format %d\n",
				FOURCC_TO_STR(fmtdesc.pixelformat),
				fmtdesc.description,
				fmtdesc.index
				);
		}
		if ((fmtdesc.pixelformat == V4L2_PIX_FMT_RGB565) ||
			 (fmtdesc.pixelformat == V4L2_PIX_FMT_RGB32) ||
			 (fmtdesc.pixelformat == V4L2_PIX_FMT_RGB24) ||
			 (fmtdesc.pixelformat == V4L2_PIX_FMT_SRGGB8) ) {
			fprintf(stdout, "Using camera format %d (%c%c%c%c)\n", i, FOURCC_TO_STR(fmtdesc.pixelformat));
			ret = fmtdesc.pixelformat;
			goto end_of_fmt;
		}
	}

	/* If no RGB565, then default to YUV422 or YUV420 */
	for (i = 0;; i++) {
		CLEAR(fmtdesc);
		fmtdesc.index = i;
		fmtdesc.type  = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		if (-1 == ioctl(v4l_fd, VIDIOC_ENUM_FMT, &fmtdesc))
			break;

		if ((fmtdesc.pixelformat == V4L2_PIX_FMT_YUYV)
			|| (fmtdesc.pixelformat == V4L2_PIX_FMT_UYVY)
			|| (fmtdesc.pixelformat == V4L2_PIX_FMT_YUV420)
			|| (fmtdesc.pixelformat == V4L2_PIX_FMT_JPEG)
			|| (fmtdesc.pixelformat == V4L2_PIX_FMT_SGRBG8)) {
			fprintf(stdout, "Using img format (%d 0x%08x): ", i, fmtdesc.pixelformat);
			if (fmtdesc.pixelformat == V4L2_PIX_FMT_YUYV)
				fprintf(stdout, "YUYV (YUV:4:2:2)\n");
			else if (fmtdesc.pixelformat == V4L2_PIX_FMT_UYVY)
				fprintf(stdout, "UYVY (YUV:4:2:2)\n");
			else if (fmtdesc.pixelformat == V4L2_PIX_FMT_YUV420)
				fprintf(stdout, "YUV:4:2:0\n");
			else if (fmtdesc.pixelformat == V4L2_PIX_FMT_JPEG)
				fprintf(stdout, "JPEG\n");
			else if (fmtdesc.pixelformat == V4L2_PIX_FMT_SGRBG8)
				fprintf(stdout, "Bayer GRBG\n");
			else
				fprintf(stdout, "??? \n");

			ret = fmtdesc.pixelformat;
			goto end_of_fmt;
		}
	}

end_of_fmt:

	fprintf(stdout, "For selected pixel format, camera supports "
				"following framesizes:\n");
	for (i = 0;; i++) {
		CLEAR(fszenum);
		fszenum.index = i;
		fszenum.pixel_format = fmtdesc.pixelformat;
		if (-1 == ioctl(v4l_fd, VIDIOC_ENUM_FRAMESIZES, &fszenum)) {
			break;
		} else {
			fprintf(stdout, "- number %d of type %d (%dx%d)\n",
				fszenum.index,
				fszenum.type,
				fszenum.discrete.width,
				fszenum.discrete.height
			);
		}
	}

	if (capture_mode) {
		CLEAR(parm);
		parm.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		parm.parm.capture.timeperframe.numerator = 1;
		if (force_fps)
			parm.parm.capture.timeperframe.denominator = force_fps;
		else
			parm.parm.capture.timeperframe.denominator = 30;
		parm.parm.capture.capturemode = capture_mode;
		fprintf(stdout, "Forcing sensor capture mode to %d\n", capture_mode);
		if (ioctl(v4l_fd, VIDIOC_S_PARM, &parm) < 0) {
			fprintf(stderr, "VIDIOC_S_PARM failed: %s", strerror(errno));
		}
	}

	return ret;
}

static void select_video_input(void)
{
	struct v4l2_input input;
	int i = 0;
	int selected_input;

	for (i = 0;; i++) {
		CLEAR(input);
		input.index = i;
		if (-1 == xioctl(v4l_fd, VIDIOC_ENUMINPUT, &input)) {
			break;
		} else {
			fprintf(stdout, "Video input %d is (\'%s\') \n",
				input.index,
				input.name
			);
		}
	}
	if (video_input) {
		/* OK, I agree, it's not really a dynamic selection ;-) */
		selected_input = video_input;
		fprintf(stdout, "Forcing video input to %d\n", selected_input);
		if (ioctl(v4l_fd, VIDIOC_S_INPUT, &selected_input) < 0) {
			fprintf(stderr, "Failed to set video input !\n");
		}
	} else {
		fprintf(stdout, "if you want to choose video input (if many), use -i option.\n");
	}
}

static void init_capture_device(unsigned int capt_width, unsigned int capt_height)
{
        struct v4l2_capability cap;
        struct v4l2_cropcap cropcap;
        struct v4l2_crop crop;
        struct v4l2_format fmt;
	unsigned int min;

        if (-1 == xioctl(v4l_fd, VIDIOC_QUERYCAP, &cap)) {
                if (EINVAL == errno) {
                        fprintf(stderr, "%s is no V4L2 device\n",
                                 dev_name);
                        exit(EXIT_FAILURE);
                } else {
                        errno_exit("VIDIOC_QUERYCAP");
                }
        }

        if (!(cap.capabilities & V4L2_CAP_VIDEO_CAPTURE)) {
                fprintf(stderr, "%s is no video capture device\n",
                         dev_name);
                exit(EXIT_FAILURE);
        }

	switch (io) {
	case IO_METHOD_READ:
		if (!(cap.capabilities & V4L2_CAP_READWRITE)) {
			fprintf(stderr, "%s does not support read i/o\n",
				 dev_name);
			exit(EXIT_FAILURE);
		}

		break;

	case IO_METHOD_MMAP:
	case IO_METHOD_USERPTR:
		if (!(cap.capabilities & V4L2_CAP_STREAMING)) {
			fprintf(stderr, "%s does not support streaming i/o\n",
				 dev_name);
			exit(EXIT_FAILURE);
		}

		break;
	}


        /* Select video input, video standard and tune here. */

	select_video_input();

	do_cropping(0, 0, 0);

        CLEAR(fmt);
	fmt.fmt.pix.pixelformat = get_a_supported_pix_fmt();
	if (force_yuv420) {
		fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_YUV420;
	}
	if (!fmt.fmt.pix.pixelformat) {
		fprintf(stderr, "%s does not support our img formats (RGB565, RGB32, YUYV, YUV420, JPEG)\n",
				dev_name);
		exit(EXIT_FAILURE);
	}

        fmt.type                = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        fmt.fmt.pix.width       = capt_width;
        fmt.fmt.pix.height      = capt_height;
        fmt.fmt.pix.field       = V4L2_FIELD_INTERLACED;

        if (-1 == xioctl(v4l_fd, VIDIOC_S_FMT, &fmt))
                errno_exit("VIDIOC_S_FMT");

        /* Note VIDIOC_S_FMT may change width and height. */
	fprintf(stdout, "Camera picture: %dx%d\n", fmt.fmt.pix.width, fmt.fmt.pix.height);

	/* Buggy driver paranoia. */
	min = fmt.fmt.pix.width * 2;
	if (fmt.fmt.pix.bytesperline < min)
		fmt.fmt.pix.bytesperline = min;
	min = fmt.fmt.pix.bytesperline * fmt.fmt.pix.height;
	if (fmt.fmt.pix.sizeimage < min)
		fmt.fmt.pix.sizeimage = min;

	mycamera.width = fmt.fmt.pix.width;
	mycamera.height = fmt.fmt.pix.height;
	mycamera.pixelformat = fmt.fmt.pix.pixelformat;
	mycamera.bytesperline = fmt.fmt.pix.bytesperline;
	mycamera.sizeimage = fmt.fmt.pix.sizeimage;

	fprintf(stdout, "    picture size: %d\n", fmt.fmt.pix.sizeimage);

	switch (io) {
	case IO_METHOD_READ:
		init_read(fmt.fmt.pix.sizeimage);
		break;

	case IO_METHOD_MMAP:
		init_mmap();
		break;

	case IO_METHOD_USERPTR:
		init_userp(fmt.fmt.pix.sizeimage);
		break;
	}
	fprintf(stdout, "    OK\n");
}

static void close_device(void)
{
        if (-1 == close(v4l_fd))
	        errno_exit("close");

        v4l_fd = -1;
}

static void open_capture_device(void)
{
        struct stat st; 

        if (-1 == stat (dev_name, &st)) {
                fprintf (stderr, "Cannot identify '%s': %d, %s\n",
                         dev_name, errno, strerror (errno));
                exit(EXIT_FAILURE);
        }

        if (!S_ISCHR (st.st_mode)) {
                fprintf(stderr, "%s is no device\n", dev_name);
                exit(EXIT_FAILURE);
        }

        v4l_fd = open(dev_name, O_RDWR /* required */ | O_NONBLOCK, 0);

        if (-1 == v4l_fd) {
                fprintf(stderr, "Cannot open '%s': %d, %s\n",
                         dev_name, errno, strerror (errno));
                exit(EXIT_FAILURE);
        }
}

static void usage(FILE *fp, int argc, char **argv)
{
	fprintf (fp,
		"Usage: %s [options]\n\n"
		"Options:\n"
		"  -c | --crop WxH        Crop camera input to WxH. In this mode, "
					"offset can be changed with arrow keys "
					"and window size with pageup/down (USB keyboard)\n"
		"  -d | --device name     Video device name [/dev/video0]\n"
		"  -h | --help            Print this message\n"
		"  -i | --input           Use given video input (if many, 0 by default)\n"
		"  -o | --capture_mode I  Use given capture mode [0-8] of sensor (if supported)\n"
		"  -m | --mmap            Use memory mapped buffers\n"
		"  -r | --read            Use read() calls\n"
		"  -u | --userp           Use application allocated buffers\n"
		"  --width W              Use W as screen width (instead of %d)\n"
		"  --height H             Use H as screen height (instead of %d)\n"
		"  --cam_width W          Use W as camera picture width (instead of %d)\n"
		"  --cam_height H         Use H as camera picture height (instead of %d)\n"
		"  --use_vpu IP           Uses processor VPU to encode video (at camera size) and send it to IP addr\n"
		"  -y | --yuv             Forces YUV420/YU12 format on camera\n"
		"  -b | --verbose         Display some debug informations\n"
		"  -f | --fps N           Choose fps (depends on camera sensor, actually 15 or 30 (default))\n"
		"",
		argv[0], SCREEN_WIDTH, SCREEN_HEIGHT, CAM_WIDTH, CAM_HEIGHT);
}

static const char short_options [] = "c:d:hi:mo:ruwtf:bxy";

static const struct option long_options [] = {
	{ "crop",	required_argument,	NULL,	'c' },
	{ "verbose",	no_argument,		NULL,	'b' },
	{ "device",	required_argument,	NULL,	'd' },
	{ "fps",	required_argument,	NULL,	'f' },
	{ "help",	no_argument,		NULL,	'h' },
	{ "input",	required_argument,	NULL,	'i' },
	{ "mmap",	no_argument,		NULL,	'm' },
	{ "capture_mode",required_argument,	NULL,	'o' },
	{ "read",	no_argument,		NULL,	'r' },
	{ "userp",	no_argument,		NULL,	'u' },
	{ "width",	required_argument,	NULL,	'w' },
	{ "height",	required_argument,	NULL,	't' },
	{ "cam_width",	required_argument,	NULL,	'x' },
	{ "yuv",	no_argument,		NULL,	'y' },
	{ "cam_height",	required_argument,	NULL,	'z' },
	{ "use_vpu",	required_argument,	NULL,	'v' },
	{ 0, 0, 0, 0 }
};

static void signal_handler(int signal)
{
	printf("Caught signal %d\n", signal);
	if (signal == SIGINT)
		go_on = 0;

	if (signal == SIGUSR1) {
		printf("Asked to start capture from external app\n");
		pause_capture = 0;
	}
	if (signal == SIGUSR2) {
		printf("Asked to stop capture from external app\n");
		pause_capture = 1;
	}
}

int main(int argc, char **argv)
{
	int ret = EXIT_SUCCESS;
	unsigned int width = SCREEN_WIDTH;
	unsigned int height = SCREEN_HEIGHT;
	unsigned int camwidth = CAM_WIDTH;
	unsigned int camheight = CAM_HEIGHT;

        dev_name = "/dev/video0";

	atexit(SDL_Quit);

	/* Do something when following signals are called */
	signal(SIGINT, signal_handler);
	signal(SIGUSR1, signal_handler);
	signal(SIGUSR2, signal_handler);

        for (;;) {
                int index;
                int c;
                
                c = getopt_long(argc, argv,
                                 short_options, long_options,
                                 &index);

                if (-1 == c)
                        break;

                switch (c) {
                case 0: /* getopt_long() flag */
                        break;

                case 'b':
                        debug = 1;
                        break;

		case 'c':
			force_crop = 1;
			sscanf(optarg, "%dx%d", &crop_width, &crop_height);
			if (debug)
				fprintf(stdout, "cropping to %dx%d\n",
						crop_width, crop_height);
			break;

                case 'd':
                        dev_name = optarg;
                        break;

		case 'f':
			force_fps = atoi(optarg);
			if (force_fps != 30)
				force_fps = 15;
			break;

                case 'h':
                        usage(stdout, argc, argv);
                        exit(EXIT_SUCCESS);

                case 'i':
			video_input = atoi(optarg);
                        break;

                case 'm':
                        io = IO_METHOD_MMAP;
			break;

                case 'o':
			capture_mode = atoi(optarg);
                        break;

                case 'r':
                        io = IO_METHOD_READ;
			break;

                case 'u':
                        io = IO_METHOD_USERPTR;
			break;

		case 'v':
			g_use_codec = 1;
			ip_addr = optarg;
			break;

		case 'w':
			width = atoi(optarg);
			break;

		case 't':
			height = atoi(optarg);
			break;

		case 'x':
			camwidth = atoi(optarg);
			break;

		case 'y':
			force_yuv420 = 1;
			break;

		case 'z':
			camheight = atoi(optarg);
			break;

                default:
                        usage(stderr, argc, argv);
                        exit(EXIT_FAILURE);
                }
        }

	camwidth = camwidth > width ? width : camwidth;
	camheight = camheight > height ? height : camheight;

	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		fprintf(stderr, "Unable to init SDL: %s\n", SDL_GetError());
		exit(EXIT_FAILURE);
	}
	fprintf(stderr, "SDL OK\n");
	screen = SDL_SetVideoMode(width, height, 0, SDL_ANYFORMAT /*| SDL_DOUBLEBUF | SDL_HWSURFACE*/);
	if (screen) {
		printf("Video mode: %dx%dx%d\n", screen->w, screen->h,
			screen->format->BitsPerPixel);
	} else {
		printf("Unable to set %dx%d video mode: %s\n", SCREEN_WIDTH, SCREEN_HEIGHT, SDL_GetError());
		ret = EXIT_FAILURE;
		goto the_end;
	}
#ifdef USE_VPU
	if (g_use_codec)
		vpu_codec_init(2, camwidth, camheight, 0 /* bitrate */, 2, ip_addr);
#endif

	open_capture_device();
	init_capture_device(camwidth, camheight);

	image = SDL_CreateRGBSurface(/*SDL_HWSURFACE*/0, mycamera.width, mycamera.height, screen->format->BitsPerPixel, 0, 0, 0, 0);
// 	printf("image fmt: RGB%d%d%d\n", image->format->Rshift, image->format->Gshift, image->format->Bshift);
	start_capturing();
	mainloop();
#ifdef USE_VPU
	if (g_use_codec)
		vpu_SystemShutdown();
#endif
	stop_capturing();

the_end:
	uninit_device();
	close_device();

	exit(ret);

	return 0;
}
