#!/bin/bash
#
# Create custom directories and links of Armadeus rootfs
#
# arg1: BR target dir
# arg2: board name
# arg3: i.MX processor type

if [ ! -d "$1" ]; then
	echo "Target directory ($1) not valid !"
	exit 1
fi

# factorizable with post_image_creation.sh ?
symlink_image()
{
	# $1 = dir, $2 = filename, $3 = symlink
	if [ -f "$1/$2" ]; then
		cd $1; ln -sf $2 $3
	fi
}

# /boot
mkdir -p $1/boot
ln -sfn . $1/boot/dtbs	# to keep compat with pre-6.0 APF6 U-Boots
symlink_image $1/boot uImage $2-linux.bin
if [ "$3" != "imx6ul" ] && [ "$3" != "imx8mm" ] && [ "$3" != "imx8mn" ] && [ "$3" != "imx93" ] && [ "$3" != "imx91" ]; then
	symlink_image $1/boot zImage $2-linux.bin
else
	if [ -f "$1/boot/zImage" ]; then
		mv $1/boot/zImage $1/boot/$2-linux.bin
	elif [ -f "$1/boot/Image" ]; then
		mv $1/boot/Image $1/boot/$2-linux.bin
	fi
fi

# !! Beware if using $2, because it can be changed for customers' BSP !!
if [ "$3" != "imxl" ]; then
	if [ "$3" == "imx6ul" ]; then
		if [ "$2" == "opos6ulnano" ]; then
			echo "OPOS6ULNANO" > $1/etc/machine
                else
			echo "OPOS6UL" > $1/etc/machine
		fi
	elif [ "$3" == "imx8mm" ]; then
		echo "OPOS8MM" > $1/etc/machine
	elif [ "$3" == "imx8mn" ]; then
		echo "OPOS8MN" > $1/etc/machine
	elif [ "$3" == "imx91" ]; then
		echo "OPOS91" > $1/etc/machine
	elif [ "$3" == "imx93" ]; then
		echo "OPOS93" > $1/etc/machine
	else
		echo "APF"${3//[!0-9]/} > $1/etc/machine
	fi
else
	echo "APF9328" > $1/etc/machine
fi

# Remove APF6 Wi-Fi init script if not building BSP for an APF6 board
if [ "$3" != "imx6" ]; then
	rm -rf $1/etc/init.d/S30wifi
fi

if [ "$3" == "imx6ul" ] || [ "$3" == "imx8mm" ] || [ "$3" == "imx8mn" ] || [ "$3" == "imx91" ] || [ "$3" == "imx93" ]; then
	# Ask to mount HOME-DATA partition at each boot:
	grep mmcblk0p3 $1/etc/fstab
	if [ "$?" != "0" ]; then
		echo "/dev/mmcblk0p3	/home		auto	defaults	0	0" >> $1/etc/fstab
	fi
fi

# Generate xxx-modules.tar.gz archive, containing (only) Linux modules
cd $1/lib/modules/
tar czf $1/../images/$2-modules.tar.gz *
cd -

exit 0
