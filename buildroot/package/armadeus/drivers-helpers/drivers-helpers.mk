################################################################################
#
# Helpers scripts for loading/using Armadeus custom Linux drivers
#
################################################################################

DRIVERS_HELPERS_VERSION = 0.6
DRIVERS_HELPERS_SITE = $(TOPDIR)/../target/linux/modules
DRIVERS_HELPERS_SITE_METHOD = local
DRIVERS_HELPERS_DEPENDENCIES = linux

DRIVERS_HELPERS_TARGET_DIR = $(TARGET_DIR)/usr/bin

define DRIVERS_HELPERS_INSTALL_TARGET_CMDS
	mkdir -p $(DRIVERS_HELPERS_TARGET_DIR)
	# loadgpio.sh often needs to be overwritten by users
	cp -f $(@D)/gpio/loadgpio.sh $(DRIVERS_HELPERS_TARGET_DIR)/
	cp -f $(@D)/gpio/gpio_helpers.sh $(DRIVERS_HELPERS_TARGET_DIR)/
	cp -f $(@D)/max1027/loadmax.sh $(DRIVERS_HELPERS_TARGET_DIR)/
#	find $(ARMADEUS-TESTSUITE_DIR) -name "*.sh" -exec install -D {} $(TARGET_DIR)/$(ARMADEUS-TESTSUITE_TARGET_DIR) \;
	cp -f $(@D)/fpga/dev_tools/loader/load_fpga $(DRIVERS_HELPERS_TARGET_DIR)/
endef

$(eval $(generic-package))
